# OTA-CLI (unofficial)


Still a big WIP! 
The only things that really work right now are commands that interact with our identity providers: "cognito" and "keycloak"    

Such commands as:   
`./ota-cli user ls` to list all users of the currently configured provider. (you  can add --idp "cognito" to list from cognito)   
`./ota-cli user info <email address>`   
or `./ota-cli user info --providerID <provider id>`   


# Configuration
You will need a file like the example_identity-provider.conf file located in `$HOME/.identity-provider.conf` in order for the cli to use environments.   
   
The conf file is hopefully self explanatory, it is managed with the ota-cli idp commands:    
`ota-cli idp get` - will display the existing environments and the asterisk indicates the current env    

`ota-cli idp set <env name>` - will make the specified environment active   






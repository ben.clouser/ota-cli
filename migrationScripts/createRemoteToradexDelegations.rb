#!/usr/bin/env ruby

require 'json'

def execute(command)
    puts command
    out = `#{command}`
    if ! $?.success?
      raise "command exited with error code #{$?.exitstatus}"
    end
    puts out
    out
end
def executeQuiet(command)
    out = `#{command}`
    if ! $?.success?
      raise "command exited with error code #{$?.exitstatus}"
    end
    out
end
def emailMatchesDomain(userEmail, domain)
    userEmailDomain = userEmail.split("@")[1]
    domain == userEmailDomain
end


    
emailDomain = nil
if ARGV.length == 1
  emailDomain = ARGV[0]
  puts "Filtering users with email domain of: #{emailDomain}" 
end

# Get list of users from keycloak
raw_users = executeQuiet "../ota-cli user ls --format json"
users = JSON.parse(raw_users)
 
# Filter users if emailDomain is specified
if emailDomain != nil 
  users = users.select{ |u| emailMatchesDomain(u["Email"], emailDomain)}
end

puts "Found #{users.length()} users to update"

users.each { |user|
    userId = user["Provider ID"]
    userNamespace = user["Namespace"] 
    userEmail = user["Email"] 

    if userNamespace.empty?
      puts "Skipping user: #{userEmail}. Reason: Missing namespace"
    else 
      puts "setting remote delegation for userId: #{userId}, namespace: #{user["Namespace"]}, email: #{user["Email"]}" 
      Array["tdx-nightly", "tdx-monthly", "tdx-quarterly", "tdx-lts", "tdx-containers"].each { |delegation| 
        execute "../ota-cli delegation remotedelegation #{delegation} https://tzn-ota-tdxota.s3.amazonaws.com/delegations/#{delegation}.json -p #{userId}" 
      }
    end
    puts "---------"
}

#!/usr/bin/env ruby

require 'json'

def execute(command)
    puts command
    out = `#{command}`
    if ! $?.success?
      raise "command exited with error code #{$?.exitstatus}"
    end
    puts out
    out
end
def executeQuiet(command)
    out = `#{command}`
    if ! $?.success?
      raise "command exited with error code #{$?.exitstatus}"
    end
    out
end
def emailMatchesDomain(userEmail, domain)
    userEmailDomain = userEmail.split("@")[1]
    domain == userEmailDomain
end

if ARGV.empty?
  puts "User email required as argument 1"
  exit 0
end


    
raw_users = executeQuiet "../ota-cli user info #{ARGV[0]}"
puts raw_users
usr = JSON.parse(raw_users)
puts usr
users = []
users.append(usr)

puts "Found #{users.length()} users"
concernedUsers = {}
usersWithOfflineUpdates = {}
users.each_with_index { |user, i|
    userId = user["id"]
    userNamespace = user["attributes"]["namespace"] 
    userEmail = user["email"] 

    if userNamespace.empty?
      puts "Skipping user ##{i}: #{userEmail}. Reason: Missing namespace"
    else 
        puts "Processing user##{i} userId: #{userId}, namespace: #{user["Namespace"]}, email: #{user["Email"]}" 
        offlineupdatesResponse = executeQuiet "../ota-cli offlineupdate ls #{userId}"
        begin
            roleNames = JSON.parse(offlineupdatesResponse)
        rescue JSON::ParserError
            puts "Failed to parse response as json for #{userEmail} and ns: #{userNamespace}. Got: #{offlineupdatesResponse}"
            puts "Skipping user, they have no offline updates"
            next
        end
        if roleNames.empty?
            puts "Skipping user, they have no offline updates"
            next
        end
        usersWithOfflineUpdates[userEmail] = roleNames.length()
        #puts JSON.pretty_generate(parsedDelegationList)
        concernedRoleNames = []
        roleNames.each { |name|
            puts "fetching #{name}"
            result = executeQuiet "../ota-cli offlineupdate get #{userId} #{name}"
            puts ""
            puts ""
            puts result
            puts "-----------"
            if result.include? "torizon"
              concernedRoleNames.append(name)
            end
          }
          if !concernedRoleNames.empty?
            concernedUsers[userEmail] = concernedRoleNames
          end
    end
}

puts ""
puts ""
puts "Users with offline updates:"
usersWithOfflineUpdates.each { |email, reason|
    puts "#{email}: #{reason}"
}
puts "Users of concern:"
concernedUsers.each { |email,reason|
  puts "#{email} : #{reason}"
}

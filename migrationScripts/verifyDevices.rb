#!/usr/bin/env ruby

require 'json'

def execute(command)
    puts command
    out = `#{command}`
    if ! $?.success?
      raise "command exited with error code #{$?.exitstatus}"
    end
    puts out
    out
end
def executeQuiet(command)
    out = `#{command}`
    if ! $?.success?
      raise "command exited with error code #{$?.exitstatus}"
    end
    out
end
def emailMatchesDomain(userEmail, domain)
    userEmailDomain = userEmail.split("@")[1]
    domain == userEmailDomain
end


# Get list of users from keycloak
raw_users = executeQuiet "../ota-cli user ls --format json"
users = JSON.parse(raw_users)
 
# # Filter users if emailDomain is specified
# if emailDomain != nil 
#   users = users.select{ |u| emailMatchesDomain(u["Email"], emailDomain)}
# end

concernedUsers = {}
    users.each{ |user| 
    userNamespace = user["Namespace"]
    userEmail = user["Email"] 

    if userNamespace.empty?
        next
    end
    # Get list of users and devices from device-registry
    devices_dev_reg_raw = executeQuiet "../ota-cli devices ls #{userNamespace}"
    devices_dev_reg = JSON.parse(devices_dev_reg_raw)
    devices_director_raw = executeQuiet "../ota-cli devices ls #{userNamespace} --director"
    devices_director = JSON.parse(devices_director_raw)

    devices_director.each { |namespace, devices|
        devices_uuid = devices.map{|d| d["uuid"]}
        devices_dev_reg_uuids = devices_dev_reg[namespace].map{|d| d["uuid"]}
        # puts "namespace " + namespace
        # puts "devices in dev reg: " + devices_dev_reg_uuids.to_s + " num devices director: " + devices_uuid.to_s
        concernedDevices = []
        devices_uuid.each { |device|
            # Check if director device doesn't exist in device-registry
            if !(devices_dev_reg_uuids.include? device)
                concernedDevices.push(device)
            end
        }
        if !concernedDevices.empty?
            concernedUsers[namespace] = {
                "email" => userEmail,
                "dev_reg_devices" => devices_dev_reg_uuids, 
                "unique_director_devices" => concernedDevices
            }
        end
    }
}

puts ""
puts "---------"
puts ""
puts ""

# concernedUsers.each { |namespace, devices|
#     val {namespace => }
# }
if concernedUsers.empty? 
    puts "No concerning users found"
else
    puts concernedUsers.to_json.to_s
end

#!/usr/bin/env ruby

require 'json'

def execute(command)
    puts command
    out = `#{command}`
    if ! $?.success?
      raise "command exited with error code #{$?.exitstatus}"
    end
    puts out
    out
end
def executeQuiet(command)
    out = `#{command}`
    if ! $?.success?
      raise "command exited with error code #{$?.exitstatus}"
    end
    out
end
def emailMatchesDomain(userEmail, domain)
    userEmailDomain = userEmail.split("@")[1]
    domain == userEmailDomain
end


    
emailDomain = nil
if ARGV.length == 1
  emailDomain = ARGV[0]
  puts "Filtering users with email domain of: #{emailDomain}" 
end

# Get list of users from keycloak
raw_users = executeQuiet "../ota-cli user ls --format json"
users = JSON.parse(raw_users)
 
# Filter users if emailDomain is specified
if emailDomain != nil 
  users = users.select{ |u| emailMatchesDomain(u["Email"], emailDomain)}
end

puts "Found #{users.length()} users"
concernedUsers = {}
users.each { |user|
    userId = user["Provider ID"]
    userNamespace = user["Namespace"] 
    userEmail = user["Email"] 

    if userNamespace.empty?
      puts "Skipping user: #{userEmail}. Reason: Missing namespace"
    else 
      puts "Delegations for userId: #{userId}, namespace: #{user["Namespace"]}, email: #{user["Email"]}" 
      delegationListResponse = executeQuiet "../ota-cli delegation ls #{userId}"
      begin
        parsedDelegationList = JSON.parse(delegationListResponse)
        #puts JSON.pretty_generate(parsedDelegationList)
        parsedDelegationList.each { |name,delegation|
          if delegation["remoteUri"].empty?
            puts "no remoteUri configured on: #{name}"
            concernedUsers[userEmail] = "missing remote url(s)"
          end
        }
      rescue JSON::ParserError
        puts delegationListResponse
        concernedUsers[userEmail] = delegationListResponse
      end
    end
    puts "---------"
}

puts ""
puts ""
concernedUsers.each { |email,reason|
  puts "#{email} : #{reason}"
}

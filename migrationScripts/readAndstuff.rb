#!/usr/bin/env ruby

require 'json'



devices_file = File.read("verifyDevices.json")
devices_json = JSON.parse(devices_file)




puts "number of users with out of sync devices #{devices_json.length}"



num_tdx_emails = 0
emails = devices_json.map{ |namespace, items| email = items["email"] }

non_toradex_emails = emails.select{|email| email.split("@")[1] != "toradex.com"}

toradex_emails = emails.select{|email| email.split("@")[1] == "toradex.com"}

puts JSON.pretty_generate(non_toradex_emails)
puts JSON.pretty_generate(toradex_emails)

# puts num_tdx_emails.to_json



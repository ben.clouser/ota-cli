#!/usr/bin/env ruby

require 'json'

def execute(command)
    $stderr.puts command
    out = `#{command}`
    if ! $?.success?
      raise "command exited with error code #{$?.exitstatus}"
    end
    #$stderr.puts out
    out
end

# Get list of clients from keycloak
raw_clients = execute "../ota-cli client search org_"
clients = JSON.parse(raw_clients)

puts "Found #{clients.length()} organization clients to update"

clients.each { |client|
    clientID = client["client_id"]
    puts "Adding role to #{clientID}..."
    execute "../ota-cli client add-attribute-mapper #{clientID} origin-namespace-mapper Namespace origin_namespace"
}

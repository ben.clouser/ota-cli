#!/usr/bin/env ruby

require 'json'
require 'set'

def execute(command)
    puts command
    out = `#{command}`
    if ! $?.success?
      raise "command exited with error code #{$?.exitstatus}"
    end
    puts out
    out
end
def executeQuiet(command)
    out = `#{command}`
    if ! $?.success?
      raise "command exited with error code #{$?.exitstatus}"
    end
    out
end
def emailMatchesDomain(userEmail, domain)
    userEmailDomain = userEmail.split("@")[1]
    domain == userEmailDomain
end


otaCliBinary = "../ota-cli"


# Get list of users from keycloak
raw_users = executeQuiet "#{otaCliBinary} user ls --format json"
users = JSON.parse(raw_users)


puts "Found #{users.length()} users"
concernedUsers = {}
betaAccessUsers = []
earlyAccessUsers = []


# These are for production
betaUsersGroupId = "9881e833-9e84-4b4c-8df2-8dffa1c5ff39"
earlyAccessGroupId = "431c9485-cb90-4d46-96b7-0a84672ac478"


# These are for dev
#betaUsersGroupId = "8c145771-c3ca-499e-80ff-4ee6b48eb902"
#earlyAccessGroupId = "f86d5320-82e1-4079-ada8-ed541a09671c"





# Start by getting the
rawEarlyAccessUsersResponse = executeQuiet "#{otaCliBinary} user groups members #{earlyAccessGroupId}"
earlyAccessUsersDetailed = JSON.parse(rawEarlyAccessUsersResponse) 
rawBetaAccessUsersResponse = executeQuiet "#{otaCliBinary} user groups members #{betaUsersGroupId}"
betaAccessUsersDetailed =  JSON.parse(rawBetaAccessUsersResponse) 

earlyAccessUsers = earlyAccessUsersDetailed.map{|user| user["email"]}
betaAccessUsers = betaAccessUsersDetailed.map{|user| user["email"]}

puts ""
puts "earlyAccessUsers from group membership:"
puts earlyAccessUsers
puts ""
puts "betaAccessUsers from group membership:"
puts betaAccessUsers
puts ""


users.each { |user|
    userId = user["Provider ID"]
    userNamespace = user["Namespace"] 
    userEmail = user["Email"] 

    if userNamespace.empty?
      puts "Skipping user: #{userEmail}. Reason: Missing namespace"
    else 
      puts "Processing for userId: #{userId}, namespace: #{user["Namespace"]}, email: #{user["Email"]}" 
      userInfoResponse = executeQuiet "#{otaCliBinary} user info #{userEmail}"
      begin
        userInfo = JSON.parse(userInfoResponse)
        # puts JSON.pretty_generate(userInfo)
        if userInfo["ClientRoles"].key?("ota-user-manager")
          userInfo["ClientRoles"]["ota-user-manager"].each { |roleName|
            # puts "Found ota-user-manager.#{roleName}"
            # puts roleName == "tdx-internal-access"
            if roleName == "tdx-internal-access"
              puts "Adding #{userEmail} to early access users because has client role: tdx-internal-access"
              earlyAccessUsers.append(userEmail)
            elsif roleName == "torizon-beta-access"
              puts "Adding #{userEmail} to beta access users because has client role: torizon-beta-access"
              betaAccessUsers.append(userEmail)
            end
          }
        end
      rescue JSON::ParserError
        puts "Failed to parse userInfo for user #{userEmail}  got"
        puts userInfoResponse
        concernedUsers[userEmail] = userInfoResponse
      end
    end
    puts "---------"
}

puts ""
puts ""
concernedUsers.each { |email,reason|
  puts "#{email} : #{reason}"
}

# Union
usersToSetBetaFlag = (betaAccessUsers | earlyAccessUsers).to_set


puts ""
puts ""
puts "Setting beta flag in db for #{usersToSetBetaFlag.length()} users:"
usersToSetBetaFlag.each { |email|
  puts "#{email}"
  #userInfoResponse = executeQuiet "#{otaCliBinary} user metadata #{email} set enableBetaFeatures  true --bool --dbHost mariadb.prod-rds-torizon.io"
  #puts userInfoResponse
}

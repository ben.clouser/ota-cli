#!/usr/bin/env ruby

require 'json'

def execute(command)
    puts command
    out = `#{command}`
    if ! $?.success?
      raise "command exited with error code #{$?.exitstatus}"
    end
    puts out
    out
end
def executeQuiet(command)
    out = `#{command}`
    if ! $?.success?
      raise "command exited with error code #{$?.exitstatus}"
    end
    out
end


if ARGV.length != 1
    puts "Specify user email as parameter"
      exit
end
userEmail = ARGV[0]

userInfo = executeQuiet "../ota-cli user info #{userEmail}"
user = JSON.parse(userInfo)
userId = user["id"]
userNamespace = user["attributes"]["namespace"] 
userEmail = user["email"] 
if userNamespace.empty?
  puts "Skipping user: #{userEmail}. Reason: Missing namespace"
else 
  puts "setting remote delegation for userId: #{userId}, namespace: #{userNamespace}, email: #{userEmail}" 
  Array["tdx-nightly", "tdx-monthly", "tdx-quarterly", "tdx-lts", "tdx-containers"].each { |delegation| 
    execute "../ota-cli delegation remotedelegation #{delegation} https://tzn-ota-tdxota.s3.amazonaws.com/delegations/#{delegation}.json -p #{userId}" 
  }
end
puts "---------"

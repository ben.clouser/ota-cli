#!/usr/bin/env ruby

require 'json'

def execute(command)
    $stderr.puts command

    out = `#{command}`

    if ! $?.success?
      raise "command exited with error code #{$?.exitstatus}"
    end

	out
end


# Get list of clients from keycloak
raw_clients = execute "../ota-cli user ls --format json"

users = JSON.parse(raw_clients)


users.each { |user|
puts "Updating user: #{user["Email"]}"
	if !user["Legacy Provider ID"].empty? 
        puts "    setting Legacy Provider ID to: #{user["Provider ID"]}"
        cmdOut = execute "../ota-cli user update #{user["Email"]} --attributes legacy_provider_id=#{user["Provider ID"]}"
        puts cmdOut
    else
        puts "    No legacy provider present. Nothing to update"
    end
}

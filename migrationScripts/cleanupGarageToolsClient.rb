#!/usr/bin/env ruby

require 'json'

def execute(command)
    $stderr.puts command

    out = `#{command}`

    if ! $?.success?
      raise "command exited with error code #{$?.exitstatus}"
    end

    #$stderr.puts out

    out
end


# Get list of clients from keycloak
raw_clients = execute "../ota-cli client ls"

clients = JSON.parse(raw_clients)


clients.each { |client|
    if client["client_id"].include? "test-user"
        clientID = client["client_id"]
        puts "Deleting #{clientID}..."
        execute "../ota-cli client delete #{clientID}"
    end
}
# pp clients


#!/usr/bin/env ruby

require 'json'
require 'parallel'

def execute(command)
    puts command
    out = `#{command}`
    if ! $?.success?
      raise "command exited with error code #{$?.exitstatus}"
    end
    puts out
    out
end
def executeQuiet(command)
    out = `#{command}  2>&1`
    if ! $?.success?
      raise "command exited with error code #{$?.exitstatus}"
    end
    out
end
def executedgaf(command)
  out = `#{command} `
  puts out
  out
end
def executequietdgaf(command)
  out = `#{command}  2>&1`
  out
end
def emailMatchesDomain(userEmail, domain)
    userEmailDomain = userEmail.split("@")[1]
    domain == userEmailDomain
end


    
emailDomain = nil
if ARGV.length == 1
  emailDomain = ARGV[0]
  puts "Filtering users with email domain of: #{emailDomain}" 
end

# Get list of users from keycloak
raw_users = executeQuiet "../ota-cli user ls --format json"
users = JSON.parse(raw_users)
 
# Filter users if emailDomain is specified
if emailDomain != nil 
  users = users.select{ |u| emailMatchesDomain(u["Email"], emailDomain)}
end

puts "Found #{users.length()} users to update"

Parallel.map(users, in_threads: 8) { |user| 
#users.each { |user|
    userId = user["Provider ID"]
    userNamespace = user["Namespace"] 
    userEmail = user["Email"] 

    output = ""
    deleteRefOut = "\n"
    addRefsOut ="\n"
    verifyOut = "\n"

    if userNamespace.empty?
      output  += "Skipping user: #{userEmail}. Reason: Missing namespace"
    else 
      output += "Updating tdx-containers trusted delegations for userId: #{userId}, namespace: #{userNamespace}, email: #{userEmail}" 
      # First delete the tdx-containers trusted delegation
      deleteRefOut = executeQuiet "curl --silent -H \"x-ats-namespace: #{userNamespace}\" -X DELETE http://127.0.0.1:8001/api/v1/namespaces/default/services/tuf-reposerver:80/proxy/api/v1/user_repo/trusted-delegations/tdx-containers"
      # Then add them all (it will only update the tdx-containers since the others already exist)
      addRefsOut = executeQuiet "../ota-cli delegation delegationrefs --providerID #{userId}"
      # Then just verify
      verifyOut = executequietdgaf "curl --silent -H \"x-ats-namespace: #{userNamespace}\" http://127.0.0.1:8001/api/v1/namespaces/default/services/tuf-reposerver:80/proxy/api/v1/user_repo/trusted-delegations | jq .[-1].paths"
    end
    output += deleteRefOut + addRefsOut + verifyOut + "---------"
    puts output
}

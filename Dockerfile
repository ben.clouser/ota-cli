FROM golang:1.16.0 as go-builder

WORKDIR /ota-cli
COPY ./ /ota-cli
#ENV GOINSECURE="gitlab.com/*,github.com,github.com/*"
ENV GOPRIVATE="gitlab.com/torizon-platform"
RUN go get ./...
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o ota-cli .

FROM ubuntu:bionic
FROM ubuntu:focal
# RUN apk --no-cache add ca-certificates
# WORKDIR /root/

COPY --from=go-builder /ota-cli/ota-cli /usr/local/bin

RUN apt-get update && apt-get install -y python3 curl unzip python3-apt python3-distutils

RUN curl -O https://bootstrap.pypa.io/get-pip.py && python3 get-pip.py --user

ENV PATH="/root/.local/bin:${PATH}"

RUN pip3 install awscli --upgrade --user
# RUN curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip" && unzip awscli-bundle.zip && ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws







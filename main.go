package main

import (
	"os"

	"github.com/rs/zerolog"
	"gitlab.com/ben.clouser/ota-cli/cmd"
	"gitlab.com/torizon-platform/sdk/env"
)

func getenv(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}
func main() {
	zerolog.SetGlobalLevel(zerolog.ErrorLevel)
	env.Config.DBHost = getenv("DB_HOST", "mariadb")
	cmd.Execute()
}

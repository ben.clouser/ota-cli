#!/usr/bin/env bash

rsync -r $HOME/workspace/toradex/ota/golang/idp/ ./idp
rsync -r $HOME/workspace/toradex/ota/golang/sdk/ ./sdk

./build-local.sh
 
#docker build . --add-host gitlab.com:10.1.8.28  --add-host app-flat.int.toradex.com:192.168.1.231 --add-host api-flat.int.toradex.com:192.168.1.231 --add-host oauth2-flat.int.toradex.com:192.168.1.231 -f ./Dockerfile --tag bclouser/ota-tdx-accounts:dev && docker push bclouser/ota-tdx-accounts:dev
docker build -f Dockerfile.dev . --network=host --tag bclouser/ota-cli:dev && docker push bclouser/ota-cli:dev

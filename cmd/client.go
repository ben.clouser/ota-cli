package cmd

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	uuid "github.com/satori/go.uuid"
	"github.com/spf13/cobra"
	"gitlab.com/ben.clouser/ota-cli/ota"
	"gitlab.com/torizon-platform/idp"
)

var fetchDetails bool = false
var detailedResp bool = false

var updateClientID string = ""
var updateClientName string = ""
var updateClientDesc string = ""
var updateClientTokenLifetime int = 0

func init() {
	rootCmd.AddCommand(clientCmd)
	clientCmd.AddCommand(listClientsCmd)
	clientCmd.PersistentFlags().StringVarP(&idpName, "idp", "i", "keycloak", "Identity Provider to use for command. Defaults to "+keycloakIDPName)
	listClientsCmd.PersistentFlags().BoolVarP(&fetchDetails, "details", "d", false, "Fetch detailed information such as secret data. This operation is expensive")
	clientCmd.AddCommand(searchClientsCommand)
	searchClientsCommand.PersistentFlags().BoolVarP(&fetchDetails, "details", "d", false, "Fetch detailed information such as secret data. This operation is expensive!")
	clientCmd.AddCommand(AddRoleMapperCmd)
	clientCmd.AddCommand(AddAttributeMapperCmd)
	clientCmd.AddCommand(DeleteClientCmd)
	clientCmd.AddCommand(DeleteManyClientsCmd)
	clientCmd.AddCommand(CreateRandomClientsCmd)
	CreateRandomClientsCmd.PersistentFlags().BoolVarP(&detailedResp, "details", "d", false, "Display detailed information for all the created clients.")
	clientCmd.AddCommand(UpdateClientCmd)
	UpdateClientCmd.PersistentFlags().StringVar(&updateClientID, "client-id", "", "New API Client ID")
	UpdateClientCmd.PersistentFlags().StringVar(&updateClientName, "name", "", "New API Client Name")
	UpdateClientCmd.PersistentFlags().StringVar(&updateClientDesc, "desc", "", "New API Client Description")
	UpdateClientCmd.PersistentFlags().IntVar(&updateClientTokenLifetime, "token-lifetime", 0, "New Access token lifetime (in seconds)")
}

var clientCmd = &cobra.Command{
	Use: "client",
}

var listClientsCmd = &cobra.Command{
	Use:   "ls",
	Short: "List all api clients in realm",
	// Args: func(cmd *cobra.Command, args []string) error {
	// 	if len(args) < 2 {
	// 		return fmt.Errorf("Invalid input. Must specify client-id & policy-name")
	// 	}
	// 	return nil
	// },
	Run: func(cmd *cobra.Command, args []string) {
		kc := idp.KeyCloak{}
		conf, _, err := ota.GetCurrentConfig(idpName)
		if err != nil {
			fmt.Println("Failed to retrieve current idp configuration")
			return
		}
		kc.Init(*conf)
		clients, err := kc.GetClients("", fetchDetails)
		if err != nil {
			fmt.Println("Failed to get list of clients. Error: " + err.Error())
			return
		}

		json, err := json.Marshal(clients)
		if err != nil {
			fmt.Println("Failed to marshal json output. Error: " + err.Error())
		}
		fmt.Println(string(json[:]))
		return
	},
}

var searchClientsCommand = &cobra.Command{
	Use:   "search <search-string>",
	Short: "search api client-ids for matching clients",
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return fmt.Errorf("Invalid input. Must specify search-query")
		}
		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		kc := idp.KeyCloak{}
		conf, _, err := ota.GetCurrentConfig(idpName)
		if err != nil {
			fmt.Println("Failed to retrieve current idp configuration")
			return
		}
		kc.Init(*conf)
		//fmt.Println("Searching clients with query: " + args[0])
		clients, err := kc.GetClients(args[0], fetchDetails)
		if err != nil {
			fmt.Println("Failed to get list of clients. Error: " + err.Error())
			return
		}

		json, err := json.Marshal(clients)
		if err != nil {
			fmt.Println("Failed to marshal json output. Error: " + err.Error())
		}
		fmt.Println(string(json[:]))
		return
	},
}

var DeleteClientCmd = &cobra.Command{
	Use:   "delete <clientID>",
	Short: "Delete client specified by clientID",
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return fmt.Errorf("Invalid input. Must specify clientID")
		}
		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		kc := idp.KeyCloak{}
		conf, _, err := ota.GetCurrentConfig(idpName)
		if err != nil {
			fmt.Println("Failed to retrieve current idp configuration")
			return
		}
		kc.Init(*conf)
		clientID := args[0]
		err = kc.DeleteClient(clientID)
		if err != nil {
			fmt.Println("failed to delete client: " + clientID + ". Error: " + err.Error())
			return
		}
		fmt.Println("Deleted client successfully")
	},
}

var DeleteManyClientsCmd = &cobra.Command{
	Use:   "delete-many <clientIDPattern>",
	Short: "Delete multiple api clients specified by the clientID search pattern. Empty pattern not allowed",
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return fmt.Errorf("invalid input. Must specify pattern")
		}
		if args[0] == "" {
			return fmt.Errorf("invalid input. ClientID pattern cannot be an empty string! (this would delete all api clients)")
		}
		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		kc := idp.KeyCloak{}
		conf, _, err := ota.GetCurrentConfig(idpName)
		if err != nil {
			fmt.Println("Failed to retrieve current idp configuration")
			return
		}
		kc.Init(*conf)
		clientIDPattern := args[0]

		clients, err := kc.GetClients(clientIDPattern, false)
		if err != nil {
			fmt.Println("Failed to retrieve api clients from keycloak server")
			return
		}
		if len(clients) == 0 {
			fmt.Println("No matching api clients exist in keycloak")
			return
		}

		fmt.Println("About to delete " + strconv.Itoa(len(clients)) + " API Clients. Are you sure [y/n]")
		sure := "n"
		fmt.Scanln(&sure)
		if sure != "y" {
			fmt.Println("OK. stopping")
			return
		}

		for _, clientToDelete := range clients {
			err = kc.DeleteClientByID(clientToDelete.ID)
			if err != nil {
				fmt.Println("failed to delete client: " + clientToDelete.ClientID + ". Error: " + err.Error())
				return
			}
		}
		fmt.Println("Deleted clients successfully")
	},
}

var CreateRandomClientsCmd = &cobra.Command{
	Use:   "create-random <api-type> <count> <namespace> [--detailed]",
	Short: "Create-random third party api clients. Useful for testing",
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) != 3 {
			return fmt.Errorf("Invalid input. Must specify api-type, count, namespace")
		}
		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		kc := idp.KeyCloak{}
		conf, _, err := ota.GetCurrentConfig(idpName)
		if err != nil {
			fmt.Println("Failed to retrieve current idp configuration")
			return
		}
		kc.Init(*conf)

		thirdPartyAPIType := args[0]
		numberOfClients, _ := strconv.Atoi(args[1])
		userNamespace := args[2]

		// Generate random api clients
		apiClientsToCreate := []idp.APIClient{}
		for i := 0; i < numberOfClients; i++ {
			apiClientsToCreate = append(apiClientsToCreate,
				idp.APIClient{ClientID: thirdPartyAPIType + "_" + uuid.NewV4().String()[:8] + "_" + userNamespace,
					Name:        "bens-api-client-test",
					Description: "Used for volume testing"})
		}

		clientsCreated, err := kc.CreateAPIClients(apiClientsToCreate)
		if err != nil {
			if len(clientsCreated) == 0 {
				fmt.Println("Failed to create any api clients. Error: " + err.Error())
				return
			}
			for _, client := range clientsCreated {
				if client.Err != nil {
					fmt.Println("Error creating clientID: " + client.ClientID + ". Error: " + client.Err.Error())
				}
			}
			return
		}
		fmt.Println("API Clients created successfully")
		if detailedResp {
			out, _ := json.MarshalIndent(clientsCreated, "", "    ")
			fmt.Println(string(out[:]))
		}
	},
}

var AddRoleMapperCmd = &cobra.Command{
	Use:   "add-role-mapper <clientID> <mapper-name> <role>",
	Short: "Add a static role mapper to specified api client",
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 3 {
			return fmt.Errorf("Invalid input. Must specify clientID, mapper-name, and role-name")
		}
		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		kc := idp.KeyCloak{}
		conf, _, err := ota.GetCurrentConfig(idpName)
		if err != nil {
			fmt.Println("Failed to retrieve current idp configuration")
			return
		}
		kc.Init(*conf)
		clientID := args[0]
		mapperName := args[1]
		roleName := args[2]

		role := idp.Role{}
		// Assume client role if format is "client-name.role-name"
		if strings.Contains(roleName, ".") {
			sections := strings.Split(roleName, ".")
			role.Client = sections[0]
			role.Name = sections[1]
		} else {
			role.Client = ""
			role.Name = roleName
		}
		fmt.Println("Adding role (" + roleName + ") mapper " + mapperName + " to client: " + clientID)
		mapperID, err := kc.CreateHardcodedRoleMapper(clientID, mapperName, role)
		if err != nil {
			fmt.Println("Failed to create role mapper. Error: " + err.Error())
			return
		}
		fmt.Println("Created role mapper successfully. ID: " + mapperID)
	},
}

var AddAttributeMapperCmd = &cobra.Command{
	Use:   "add-attribute-mapper <clientID> <mapper-name> <user-attribute> <claim-name>",
	Short: "Add a user attribute mapper to a specified api client",
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 4 {
			return fmt.Errorf("Invalid input. Must specify clientID, mapper-name, user-attribute, and claim-name")
		}
		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		kc := idp.KeyCloak{}
		conf, _, err := ota.GetCurrentConfig(idpName)
		if err != nil {
			fmt.Println("Failed to retrieve current idp configuration")
			return
		}
		kc.Init(*conf)
		clientID := args[0]
		mapperName := args[1]
		userAttribute := args[2]
		claimName := args[3]
		fmt.Println("Adding attribute mapper " + mapperName + " to client: " + clientID)
		mapperID, err := kc.CreateUserAttributeMapper(clientID, mapperName, claimName, userAttribute)
		if err != nil {
			fmt.Println("Failed to add user attribute mapper. Error: " + err.Error())
			return
		}
		fmt.Println("Added user attribute mapper to client successfully. ID: " + mapperID)
	},
}

var UpdateClientCmd = &cobra.Command{
	Use:   "update <clientID> [ --client-id <new client-id> --name <new name> --desc <new desc> --token-lifetime <new token lifetime seconds>",
	Short: "Update client specified by clientID",
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return fmt.Errorf("Invalid input. Must specify clientID")
		}
		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		kc := idp.KeyCloak{}
		conf, _, err := ota.GetCurrentConfig(idpName)
		if err != nil {
			fmt.Println("Failed to retrieve current idp configuration")
			return
		}
		kc.Init(*conf)
		clientID := args[0]
		modifiedClient := idp.APIClient{
			ClientID:                   updateClientID,
			Name:                       updateClientName,
			Description:                updateClientDesc,
			AccessTokenLifeTimeSeconds: updateClientTokenLifetime,
		}

		err = kc.UpdateAPIClient(clientID, modifiedClient)
		if err != nil {
			fmt.Println("failed to update client: " + clientID + ". Error: " + err.Error())
			return
		}
		fmt.Println("Updated client successfully")
	},
}

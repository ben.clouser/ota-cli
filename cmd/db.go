package cmd

import (
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/ben.clouser/ota-cli/ota"
	"gitlab.com/torizon-platform/idp"
	"gitlab.com/torizon-platform/sdk/dao"
	"gitlab.com/torizon-platform/sdk/env"
	"gitlab.com/torizon-platform/sdk/models"
	"gitlab.com/torizon-platform/sdk/tdxerr"
	"gitlab.com/torizon-platform/sdk/user"
)

var namespace string
var email string
var all bool

func init() {
	rootCmd.AddCommand(dbCmd)
	dbCmd.AddCommand(updateUserMetadataToUseNamespaceCmd)
	updateUserMetadataToUseNamespaceCmd.Flags().StringVarP(&providerID, "provider-id", "i", "", "User's providerID")
	updateUserMetadataToUseNamespaceCmd.Flags().StringVarP(&dbHost, "dbHost", "d", "mariadb", "Database host to connect. Defaults to mariadb")
	updateUserMetadataToUseNamespaceCmd.Flags().StringVarP(&dbUser, "dbUser", "b", "root", "Database user to connect as. Defaults to root")
	updateUserMetadataToUseNamespaceCmd.Flags().StringVarP(&dbPass, "dbPass", "p", "root", "Database password for user. Defaults to root")
	dbCmd.AddCommand(updateUserProviderID)
	updateUserProviderID.Flags().BoolVarP(&all, "all", "", false, "Process all users in idp")
	updateUserProviderID.Flags().StringVarP(&namespace, "namespace", "n", "", "User's namespace")
	updateUserProviderID.Flags().StringVarP(&email, "email", "e", "", "User's email")
	updateUserProviderID.Flags().StringVarP(&providerID, "provider-id", "i", "", "User's providerID")
	updateUserProviderID.Flags().StringVarP(&dbHost, "dbHost", "d", "mariadb", "Database host to connect. Defaults to mariadb")
	updateUserProviderID.Flags().StringVarP(&dbUser, "dbUser", "b", "root", "Database user to connect as. Defaults to root")
	updateUserProviderID.Flags().StringVarP(&dbPass, "dbPass", "p", "root", "Database password for user. Defaults to root")

}

var dbCmd = &cobra.Command{
	Use:   "db",
	Short: "direct database modifications, migrations, and updates. Requires running in-cluster",
}

var updateUserMetadataToUseNamespaceCmd = &cobra.Command{
	Use:   "update-metadata [--providerID,p]",
	Short: "populate namespace column of all items in user_registry.metadata table with user namespace matching provider in keycloak",
	Run: func(cmd *cobra.Command, args []string) {

		fmt.Println("DB Connection settings: " + dbUser + ":" + dbPass + "@" + dbHost)
		env.Config.DBHost = dbHost
		fmt.Println("Setting db host to: " + env.Config.DBHost)

		users, err := ota.GetUserList(idpName, -1)
		if err != nil {
			fmt.Println("Failed to get users: " + err.Error())
			return
		}
		for _, currentUser := range users {
			if currentUser.Attributes.Namespace == "" {
				fmt.Println("user (" + currentUser.Email + ") has no namespace... skipping")
				continue
			}
			fmt.Println("Processing user (" + currentUser.Email + ") namespace: " + currentUser.Attributes.Namespace)
			// Lookup User metadata by user's providerID
			providerID := ""
			if currentUser.Attributes.LegacyProviderID != "" {
				fmt.Println("Using legacy providerID")
				providerID = currentUser.Attributes.LegacyProviderID
			} else {
				fmt.Println("Using Torizon provider ID")
				providerID = currentUser.ID
			}
			userInfo, err := getUserMetadataByProviderID(providerID)
			if err != nil {
				if txErr, ok := err.(tdxerr.Error); ok {
					if txErr.Code() == tdxerr.ErrCodeRecordNotFound {
						fmt.Println("Ok, so we didn't find any user metadata for providerID: " + providerID)
						continue
					}
				} else {
					fmt.Println("Failed to get user metadata for Provider ID: " + providerID + " error: " + err.Error())
					return
				}
			}
			if len(userInfo.Meta) == 0 {
				fmt.Println("Skipping because user doesn't have any metadata artifacts")
				continue
			}
			// Actually update the namespace
			fmt.Println("Adding namespace to providerID: " + providerID + " ... userctx.pID: " + userInfo.ProviderID)
			err = addNamespaceToUserMetadata(userInfo.ProviderID, userInfo.UserNamespace)
			if err != nil {
				fmt.Println("Failed to add user to namespace metadata. Error: " + err.Error())
				fmt.Println("Stopping")
				return
			}
		}
	},
}

func getUserMetadataByProviderID(providerID string) (*models.User, error) {
	userMgr := user.User{DBActions: &user.UserDBActions{FetchMetas: true}, ExtActions: &user.UserExtActions{}}
	foundUser, err := userMgr.GetByProviderID(providerID)
	if err != nil {
		fmt.Println("Failed to retrieve user info with metadata from database. Error: " + err.Error())
		return nil, err
	}
	return foundUser, nil
}

func addNamespaceToUserMetadata(providerID string, namespace string) error {
	db := dao.GetDbHandle("user_registry")
	db.AutoMigrate(&models.Metadata{})
	meta := models.Metadata{UserNamespace: namespace}
	update := db.Model(&meta).Where("providerId = ?", providerID).Updates(&meta)
	affected := update.RowsAffected
	var createErr error
	if affected == 0 {
		createErr = db.Create(&meta).Error
	}
	if update.Error == nil && createErr == nil {
		if affected == 0 {
			fmt.Println("created new")
		} else {
			fmt.Println("updated " + strconv.FormatInt(affected, 10))
		}
	}
	return nil
}

func dbUpdateUserProviderID(namespace string, providerID string) error {
	db := dao.GetDbHandle("user_registry")
	db.AutoMigrate(&models.User{})
	user := models.User{ProviderID: providerID}
	update := db.Model(&user).Where("userNamespace = ?", namespace).Update(&user)
	if update.Error != nil {
		return update.Error
	}
	fmt.Println("updated " + strconv.FormatInt(update.RowsAffected, 10) + " rows")
	return nil
}

var updateUserProviderID = &cobra.Command{
	Use:   "update-provider-id <--namespace|-n || --email|-e || --all> <--provider-id|-i>",
	Short: "Update user_registry.Users:providerID column to match keycloak providerID of the keycloak user specified by namespace or email.",
	Run: func(cmd *cobra.Command, args []string) {
		if !all && namespace == "" && email == "" {
			fmt.Println("all, namespace, or email must be specified!")
			return
		}
		if !all && providerID == "" {
			fmt.Println("provider-id must be specified unless all is specified")
			return
		}
		fmt.Println("DB Connection settings: " + dbUser + ":" + dbPass + "@" + dbHost)
		env.Config.DBHost = dbHost
		usersToUpdate := []*idp.User{}
		if all {
			allUsers, err := ota.GetUserList(idpName, -1)
			if err != nil {
				fmt.Println("Failed to get users from idp: " + err.Error())
				return
			}
			usersToUpdate = allUsers
		} else if email != "" {
			user, err := ota.GetUserByEmail(idpName, email)
			if err != nil {
				fmt.Println("Failed to get users from idp: " + err.Error())
				return
			}
			usersToUpdate = append(usersToUpdate, user)
		} else {
			users, err := ota.GetUserList(idpName, -1)
			if err != nil {
				fmt.Println("Failed to get users: " + err.Error())
				return
			}

			for _, currentUser := range users {
				if currentUser.Attributes.Namespace == namespace {
					usersToUpdate = append(usersToUpdate, currentUser)
					break
				}
			}
		}
		if len(usersToUpdate) == 0 {
			fmt.Println("Failed to find specified user in idp")
			return
		}

		for _, user := range usersToUpdate {
			fmt.Println("Updating user (namespace: " + user.Attributes.Namespace + ") with providerID: " + user.ID)
			err := dbUpdateUserProviderID(user.Attributes.Namespace, user.ID)
			if err != nil {
				fmt.Println("failed to update user (namespace: " + user.Attributes.Namespace + ") with providerID: " + user.ID)
				return
			}
		}
	},
}

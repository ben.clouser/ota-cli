package cmd

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/ben.clouser/ota-cli/ota"
)

var fetchDirector = false
var allUsers = false

func init() {
	rootCmd.AddCommand(devicesCmd)
	devicesCmd.AddCommand(listDevicesCmd)
	listDevicesCmd.Flags().BoolVarP(&fetchDirector, "director", "d", false, "Fetch devices from director")
	listDevicesCmd.Flags().BoolVarP(&allUsers, "all", "a", false, "Fetch devices for all users")
}

var devicesCmd = &cobra.Command{
	Use: "devices",
}

var listDevicesCmd = &cobra.Command{
	Use:   "ls <namespace|--all|-a> [--director]",
	Short: "List all of a users devices. REQUIRES ACTIVE KUBERNETES PROXY!",
	Run: func(cmd *cobra.Command, args []string) {
		const deviceRegistryUrl = "http://127.0.0.1:8001/api/v1/namespaces/default/services/device-registry:80/proxy/"
		const directorUrl = "http://127.0.0.1:8001/api/v1/namespaces/default/services/director:80/proxy/"
		const namespaceHeaderKey = "x-ats-namespace"

		users := []string{}
		userNamespace := ""
		if allUsers {
			idpUsers, err := ota.GetUserList(idpName, -1)
			if err != nil {
				log.Fatal(err.Error())
			}
			if len(idpUsers) == 0 {
				log.Fatal("No users returned from provider")
			}
			for _, user := range idpUsers {
				users = append(users, user.Attributes.Namespace)
			}
		} else {
			if len(args) < 1 {
				log.Fatal("Invalid input. Must specify user namespace as arg 1 or pass the --all flag")
			}
			userNamespace = args[0]
			users = append(users, userNamespace)
		}

		client := &http.Client{}

		type Device struct {
			Namespace    string `json:"namespace"`
			Uuid         string `json:"uuid"`
			DeviceName   string `json:"deviceName"`
			DeviceId     string `json:"deviceId"`
			DeviceType   string `json:"deviceType"`
			LastSeen     string `json:"lastSeen"`
			CreatedAt    string `json:"createdAt"`
			ActivatedAt  string `json:"activatedAt"`
			DeviceStatus string `json:"deviceStatus"`
			Notes        string `json:"notes"`
		}

		type PaginatedDevices struct {
			Values []Device
			Total  int
			Offset int
			Limit  int
		}

		type PagedDeviceUuids struct {
			Values []string
			Total  int
			Offset int
			Limit  int
		}

		userResps := make(map[string][]Device)
		for _, user := range users {
			if !fetchDirector {
				req, err := http.NewRequest("GET", deviceRegistryUrl+"api/v1/devices", nil)
				if err != nil {
					log.Fatal("Failed to create http request. Error: " + err.Error())
				}
				req.Header.Add(namespaceHeaderKey, user)
				resp, err := client.Do(req)
				if err != nil {
					log.Fatal("Failed to send http request. Error: " + err.Error())
				}

				buf := new(bytes.Buffer)
				buf.ReadFrom(resp.Body)

				var pagedDevices = PaginatedDevices{}
				err = json.Unmarshal(buf.Bytes(), &pagedDevices)
				if err != nil {
					log.Fatal("Failed to unmarshal json output. Error: " + err.Error())
				}
				userResps[user] = pagedDevices.Values
			} else {
				req, err := http.NewRequest("GET", directorUrl+"api/v1/admin/devices", nil)
				if err != nil {
					log.Fatal("Failed to create http request. Error: " + err.Error())
				}
				req.Header.Add(namespaceHeaderKey, user)
				resp, err := client.Do(req)
				if err != nil {
					log.Fatal("Failed to send http request. Error: " + err.Error())
				}

				buf := new(bytes.Buffer)
				buf.ReadFrom(resp.Body)

				pagedDeviceUuids := PagedDeviceUuids{}
				err = json.Unmarshal(buf.Bytes(), &pagedDeviceUuids)
				if err != nil {
					log.Fatal("Failed to unmarshal json output. Error: " + err.Error())
				}

				devices := []Device{}
				for _, device := range pagedDeviceUuids.Values {
					devices = append(devices, Device{Uuid: device})
				}
				userResps[user] = devices
			}
		}
		json.NewEncoder(os.Stdout).Encode(userResps)
		// fmt.Println(userResps)
	},
}

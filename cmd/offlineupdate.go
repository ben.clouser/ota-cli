package cmd

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/ben.clouser/ota-cli/ota"
)

func init() {
	rootCmd.AddCommand(offlineupdateCmd)
	offlineupdateCmd.AddCommand(offlineupdateListCmd)
	offlineupdateCmd.AddCommand(addDelegationRefsCmd)
	offlineupdateCmd.AddCommand(offlineupdateGetCmd)

}

func getRequestNamespace(url string, namespace string) (err error, status int, body string) {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		fmt.Printf("client: could not create request: %s\n", err)
		return err, 0, ""
	}
	req.Header.Add("x-ats-namespace", namespace)

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Printf("client: error making http request: %s\n", err)
		return err, 0, ""
	}

	resBody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Printf("client: could not read response body: %s\n", err)
		return err, 0, ""
	}
	return nil, res.StatusCode, string(resBody[:])
}

var offlineupdateCmd = &cobra.Command{
	Use:   "offlineupdate",
	Short: "Commands pertaining to a user's offline update metadata",
}

type Snapshot struct {
	Hashes  map[string]string
	Length  int
	Version int
}
type UnsignedRole struct {
	Meta    map[string]Snapshot
	Expires string
	Version int
	Type    string
}
type OfflineSnapshots struct {
	Signatures []map[string]string
	Signed     UnsignedRole
}

var offlineupdateListCmd = &cobra.Command{
	Use:   "ls <providerId>",
	Short: "List delegations in a user's repository. REQUIRES ACTIVE KUBERNETES PROXY",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 {
			fmt.Println("User provider id must be provided as argument 1")
			return
		}
		idpUser, err := ota.GetUser(idpName, args[0])
		if err != nil {
			fmt.Println("Failed to get user information from provider, Error: " + err.Error())
			return
		}
		directorSvcUrl := "http://127.0.0.1:8001/api/v1/namespaces/default/services/director:80/proxy"
		offlinesnapshotUrl := directorSvcUrl + "/api/v1/admin/repo/offline-snapshot.json"
		err, code, body := getRequestNamespace(offlinesnapshotUrl, idpUser.Attributes.Namespace)
		if err != nil {
			fmt.Println("Failed to create offline updates request. Error: " + err.Error())
			return
		}
		if code != 200 {
			fmt.Println("Failed to get offline updates. Got code: " + strconv.FormatInt(int64(code), 10) + " and body: " + body)
			return
		}
		// for short output of just names
		rolenames := []string{}
		snapshots := OfflineSnapshots{}
		json.Unmarshal([]byte(body), &snapshots)
		for k, _ := range snapshots.Signed.Meta {
			rolenames = append(rolenames, strings.Split(k, ".")[0])
			// fmt.Println(k)
		}
		out, _ := json.Marshal(rolenames)
		fmt.Println(string(out[:]))
	},
}

var offlineupdateGetCmd = &cobra.Command{
	Use:   "get <providerId> <offfline-update-role-name",
	Short: "Fetch offline update role metadata. REQUIRES ACTIVE KUBERNETES PROXY",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 {
			fmt.Println("User provider id must be provided as argument 1")
			fmt.Println("Metadata role name must be provided as argument 2")
			return
		}
		if len(args) < 2 {
			fmt.Println("Metadata role name must be provided as argument 2")
			return
		}
		idpUser, err := ota.GetUser(idpName, args[0])
		if err != nil {
			fmt.Println("Failed to get user information from provider, Error: " + err.Error())
			return
		}
		directorSvcUrl := "http://127.0.0.1:8001/api/v1/namespaces/default/services/director:80/proxy"
		offlinesnapshotUrl := directorSvcUrl + "/api/v1/admin/repo/offline-updates/" + args[1] + ".json"
		err, code, body := getRequestNamespace(offlinesnapshotUrl, idpUser.Attributes.Namespace)
		if err != nil {
			fmt.Println("Failed to create offline updates request. Error: " + err.Error())
			return
		}
		if code != 200 {
			fmt.Println("Failed to get offline update. Got code: " + strconv.FormatInt(int64(code), 10) + " and body: " + body)
			return
		}
		// snapshots := OfflineSnapshots{}
		// json.Unmarshal([]byte(body), &snapshots)
		// for k, _ := range snapshots.Signed.Meta {
		// 	fmt.Println(k)
		// }
		fmt.Println(body)
	},
}

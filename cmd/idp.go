package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/ben.clouser/ota-cli/ota"
)

func init() {
	rootCmd.AddCommand(idpCmd)
	idpCmd.AddCommand(providerGetCmd)
	idpCmd.AddCommand(providerSetCmd)
}

var idpCmd = &cobra.Command{
	Use: "idp",
}

var providerGetCmd = &cobra.Command{
	Use:   "get",
	Short: "Get and show identity providers end environments configured for use by this tool",
	Run: func(cmd *cobra.Command, args []string) {
		kcConf, err := ota.GetIDPConfig("keycloak")
		if err != nil {
			fmt.Println("Failed to get keycloak configuration, Error: " + err.Error())
			return
		}
		fmt.Println("CURRENT      IDP        ENV       REALM/USER-POOL           URI  ")
		for env, conf := range (*kcConf).Envs {
			current := " "
			if (*kcConf).CurrentEnv == env {
				current = "*"
			}
			fmt.Println("   " + current + "      keycloak     " + env + "      " + conf.KC_Realm + "           " + conf.KC_ProviderUri)
		}

		fmt.Println("------------------------------------------------------------------")
		cognitoConf, err := ota.GetIDPConfig("cognito")
		if err != nil {
			fmt.Println("Failed to get cognito configuration, Error: " + err.Error())
			return
		}
		for env, conf := range (*cognitoConf).Envs {
			current := " "
			if (*cognitoConf).CurrentEnv == env {
				current = "*"
			}
			fmt.Println("   " + current + "      cognito     " + env + "      " + conf.AwsUserPool + "     - ")
		}
	},
}

var providerSetCmd = &cobra.Command{
	Use:   "set <idp name> <env>",
	Short: "set the actively configured environment for a specific identity provider",
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 2 {
			return fmt.Errorf("Must specify the identity provider name as arg1, and environment name as arg2")
		}
		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		idpName := args[0]
		env := args[1]
		err := ota.SetCurrentIDPEnv(idpName, env)
		if err != nil {
			fmt.Println("Failed to set idp env: " + err.Error())
			return
		}
		fmt.Println("Successfully set provider: " + idpName + ", to use environment: " + env)
	},
}

package cmd

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/ben.clouser/ota-cli/ota"
	"gitlab.com/torizon-platform/idp"
)

var detailedResults bool

func init() {
	rootCmd.AddCommand(authCmd)
	authCmd.AddCommand(getPolicyCmd)
	authCmd.PersistentFlags().StringVarP(&idpName, "idp", "i", "keycloak", "Identity Provider to use for command. Defaults to "+keycloakIDPName)
	getPolicyCmd.Flags().BoolVarP(&detailedResults, "detailed", "d", false, "Fetchs info for clients/roles listed in policy")

	authCmd.AddCommand(updatePolicyCmd)
}

var authCmd = &cobra.Command{
	Use: "auth",
}

var getPolicyCmd = &cobra.Command{
	Use:   "get-policy <resource-server-client-id> <policy-name>",
	Short: "Show specified policy",
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 2 {
			return fmt.Errorf("Invalid input. Must specify client-id & policy-name")
		}
		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		clientID := args[0]
		policyID := args[1]
		kc := idp.KeyCloak{}
		conf, _, err := ota.GetCurrentConfig(idpName)
		if err != nil {
			fmt.Println("Failed to retrieve current idp configuration")
			return
		}
		kc.Init(*conf)
		policy, err := kc.GetPolicy(clientID, policyID)
		if err != nil {
			fmt.Println("Failed to get specified policy. Error: " + err.Error())
			return
		}

		if detailedResults {
			output := struct {
				ID      string          `json:"id"`
				Name    string          `json:"name"`
				Type    string          `json:"type"`
				Clients []idp.APIClient `json:"clients,omitempty"`
				Roles   []idp.Role      `json:"roles,omitempty"`
			}{}
			if policy.Type == "role" {
				// fetch information for all roles
			} else if policy.Type == "client" {
				// Fetch information for all clients
				clients, errors := kc.GetClientsByID(policy.Clients)
				if len(errors) != 0 {
					if len(clients) == 0 {
						fmt.Println("Failed to retrieve any clients. Error:  " + errors[0].Error())
						return
					}
					for _, err := range errors {
						fmt.Println("Failed to retrieve client " + err.Error())
					}
				}
				fmt.Println("GOt back " + strconv.Itoa(len(clients)) + " clients data")
				output.ID = policy.ID
				output.Name = policy.Name
				output.Type = policy.Type
				output.Clients = clients
			}
			json, err := json.Marshal(output)
			if err != nil {
				fmt.Println("Failed to marshal detailed output. Error: " + err.Error())
			}
			fmt.Println(string(json[:]))
		} else {
			json, err := json.Marshal(policy)
			if err != nil {
				fmt.Println("Failed to marshal the whole thing")
			}
			fmt.Println(string(json[:]))
		}
		return
	},
}

var updatePolicyCmd = &cobra.Command{
	Use:   "update-policy <resource-server-client-id> <policy-name> <rm> <id-of-client|id-of-role|id-of-user>",
	Short: "Update specified policy",
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 4 {
			return fmt.Errorf("Invalid input. Must specify client-id, policy-name, command, & id")
		}
		if args[2] != "rm" {
			return fmt.Errorf("bad command specified, only 'rm' is currently supported")
		}
		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		clientID := args[0]
		policyID := args[1]
		policyCmd := args[2]
		entityID := args[3]
		kc := idp.KeyCloak{}
		conf, _, err := ota.GetCurrentConfig(idpName)
		if err != nil {
			fmt.Println("Failed to retrieve current idp configuration")
			return
		}
		kc.Init(*conf)

		policy, err := kc.GetPolicy(clientID, policyID)
		if err != nil {
			fmt.Println("Unable to retrieve policy: " + err.Error())
			return
		}
		if policy.Type == "client" && policyCmd == "rm" {
			err := kc.RemoveClientsFromClientPolicy(clientID, policyID, []string{entityID})
			if err != nil {
				fmt.Println("Failed to remove clients from client policy. Error: " + err.Error())
				return
			}
		} else if policy.Type == "role" {
			fmt.Println("Updating role policies isn't yet supported")
			return
		} else if policy.Type == "user" {
			fmt.Println("Updating user policies isn't yet supported")
			return
		}
	},
}

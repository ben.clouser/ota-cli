package cmd

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"sort"
	"strconv"
	"strings"
	"text/tabwriter"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/ben.clouser/ota-cli/ota"

	// Yeah, we have gotten past the point of a common idp interface... this will become a mess quickly
	"gitlab.com/torizon-platform/idp"
	"gitlab.com/torizon-platform/sdk/dao"
	"gitlab.com/torizon-platform/sdk/env"
	"gitlab.com/torizon-platform/sdk/models"
	userInterface "gitlab.com/torizon-platform/sdk/user"
)

var providerID string
var allFlag, onlyNamespace, overwrite bool
var allDomainFlag string
var userPool string
var idpName string
var sortByCreationTime bool
var dbHost, dbUser, dbPass string
var outputFormat string
var onlyAttributes string
var enabled string
var window int
var onlyUnverified bool
var onlyVerified bool
var skipDomain string
var remoteHeaders string
var valueBool bool

const keycloakIDPName string = "keycloak"
const cognitoIDPName string = "cognito"
const DelegatedUserNamespace string = "default"

var attributes string

func init() {
	rootCmd.AddCommand(userCmd)
	userCmd.PersistentFlags().StringVarP(&idpName, "idp", "i", keycloakIDPName, "Identity Provider to use for command. Defaults to "+keycloakIDPName)
	userCmd.AddCommand(userListCmd)
	userListCmd.Flags().BoolVarP(&sortByCreationTime, "time", "t", false, "Sort by creation time. Newest first")
	userListCmd.Flags().BoolVarP(&onlyUnverified, "unverified", "", false, "Filter by users that haven't verified their email address")
	userListCmd.Flags().BoolVarP(&onlyVerified, "verified", "", false, "Filter by users that have verified their email address")
	userListCmd.Flags().IntVarP(&window, "window", "w", 0, "Some number of days in the past to limit results. ie users created in the last 20 days")
	userListCmd.Flags().StringVarP(&dbHost, "dbHost", "d", "mariadb", "Database host to connect. Defaults to mariadb")
	userListCmd.Flags().StringVarP(&skipDomain, "skip-domain", "", "", "Filter users by skipping the specified email domain when parsing users")
	userListCmd.Flags().StringVarP(&dbUser, "dbUser", "b", "root", "Database user to connect as. Defaults to root")
	userListCmd.Flags().StringVarP(&dbPass, "dbPass", "p", "root", "Database password for user. Defaults to root")
	userListCmd.Flags().StringVarP(&outputFormat, "format", "", "", "Output format. [json|csv]")
	userListCmd.Flags().StringVarP(&onlyAttributes, "attributes", "", "", "csv list of attributes to return for each user \"[Email,First Name,Last Name,Company,Country,Namespace,Created Date,Migrated]\"")
	userCmd.AddCommand(userInfoCmd)
	userInfoCmd.Flags().StringVarP(&providerID, "providerID", "p", "", "Provider ID - Assumes user already exists in provider (AWS cognito)")
	userCmd.AddCommand(userCreateCmd)
	userCreateCmd.Flags().StringVarP(&providerID, "providerID", "p", "", "Provider ID - Assumes user already exists in provider (AWS cognito)")
	userCreateCmd.Flags().BoolVarP(&allFlag, "all", "", false, "All - Assumes users already exists in provider (AWS cognito)")
	userCreateCmd.Flags().StringVarP(&allDomainFlag, "all-domain", "", "", "All matching specified domain- Assumes users already exists in provider (AWS cognito)")
	userCmd.AddCommand(userDeleteCmd)
	userDeleteCmd.Flags().StringVarP(&providerID, "providerID", "p", "", "Provider ID - Assumes user already exists in provider (AWS cognito)")
	userDeleteCmd.Flags().BoolVarP(&allFlag, "all", "", false, "All - Assumes users already exists in provider (AWS cognito)")
	userDeleteCmd.Flags().StringVarP(&allDomainFlag, "all-domain", "", "", "All matching specified domain- Assumes users already exists in provider (AWS cognito)")
	userDeleteCmd.Flags().StringVarP(&userPool, "userPool", "u", string(ota.ProdPool), "Cognito user pool. dev or prod.  Default: prod")
	userCmd.AddCommand(userMigrateToKeycloakCmd)
	userMigrateToKeycloakCmd.Flags().StringVarP(&providerID, "providerID", "", "", "Provider ID - Assumes user already exists in provider (AWS cognito)")
	userMigrateToKeycloakCmd.Flags().BoolVarP(&allFlag, "all", "", false, "All - Migrate all users in cognito to keycloak")
	userMigrateToKeycloakCmd.Flags().BoolVarP(&onlyNamespace, "only-namespace", "", false, "only-namespace - only migrate the namespace attribute (useful for syncing individual users after the fact. Can overwrite user's existing namespace in keycloak if paired with 'overwrite' flag")
	userMigrateToKeycloakCmd.Flags().BoolVarP(&overwrite, "overwrite", "", false, "overwrite - to be used with the onlyns flag. If set this will overwrite an existing user's namespace in keycloak with the one found in database")
	userMigrateToKeycloakCmd.Flags().StringVarP(&dbHost, "dbHost", "d", "mariadb", "Database host to connect. Defaults to mariadb")
	userMigrateToKeycloakCmd.Flags().StringVarP(&dbUser, "dbUser", "b", "root", "Database user to connect as. Defaults to root")
	userMigrateToKeycloakCmd.Flags().StringVarP(&dbPass, "dbPass", "p", "root", "Database password for user. Defaults to root")
	userCmd.AddCommand(updateUserCmd)
	updateUserCmd.Flags().StringVarP(&enabled, "enabled", "", "", "Set whether a user is enabled")
	updateUserCmd.Flags().StringVarP(&attributes, "attributes", "", "", "pairs of user attributes to update. ie: key1=value1,key2=value2")
	userCmd.AddCommand(metadataCmd)
	metadataCmd.Flags().StringVarP(&dbHost, "dbHost", "d", "mariadb", "Database host to connect. Defaults to mariadb")
	metadataCmd.Flags().BoolVarP(&valueBool, "bool", "b", false, "Treat incoming field as a boolean instead of a string")
	userCmd.AddCommand(groupsCmd)

	// add command to add clients to policy?
}

var userCmd = &cobra.Command{
	Use: "user",
}

func userWantsToContinue() bool {
	fmt.Println("Do you want to continue (y/n)?")
	var yesNo string
	fmt.Scanln(&yesNo)
	if yesNo == "n" {
		fmt.Println("Stopping here.")
		return false
	}
	return true
}
func parseCreatedTime(timestamp float64) time.Time {
	seconds := int64(timestamp)
	return time.Unix(seconds, 0)
}

func userMigrated(user *ota.IDPUserInfo) bool {
	for _, action := range (*user).RequiredActions {
		if action == "UPDATE_PROFILE" {
			return false
		}
	}
	return true
}

func userSubscriptionTier(user *ota.IDPUserInfo) string {
	for clientName, roles := range (*user).ClientRoles {
		if clientName == "ota-user-manager" {
			for _, role := range roles {
				switch role {
				case "torizon-free-tier":
					return "free"
				case "torizon-commercial-tier":
					return "commercial"
				case "torizon-advanced-tier":
					return "advanced"
				case "torizon-enterprise-tier":
					return "enterprise"
				}
			}
		}
	}
	return "unknown"
}

// lookupField will return the value from the struct matching given field name
func lookupField(user UserOutput, fieldName string) string {
	switch fieldName {
	case "First Name":
		return user.FirstName
	case "Last Name":
		return user.LastName
	case "Email":
		return user.Email
	case "Company":
		return user.Company
	case "Country":
		return user.Country
	case "Namespace":
		return user.Namespace
	case "Provider ID":
		return user.ProviderID
	case "Legacy Provider ID":
		return user.LegacyProviderID
	case "Created Date":
		return user.CreatedDate
	case "Migrated":
		return strconv.FormatBool(user.Migrated)
	default:
		return ""
	}
}

type UserOutput struct {
	ProviderID       string `json:"provider_id"`
	FirstName        string `json"first_name,omitempty"`
	LastName         string `json"last_name,omitempty"`
	Email            string `json:"email,omitempty"`
	Company          string `json:"company,omitempty"`
	Country          string `json:"country,omitempty"`
	Namespace        string `json:"namespace,omitempty"`
	LegacyProviderID string `json:"legacy_provider_id,omitempty"`
	CreatedDate      string `json:"created_date,omitempty"`
	Migrated         bool   `json:"migrated,omitempty"`
}

var userListCmd = &cobra.Command{
	Use:   "ls [-u <dev|prod>]",
	Short: "List users registered in AWS cognito",
	Run: func(cmd *cobra.Command, args []string) {
		db, _ := strconv.ParseBool(cmd.Flags().Lookup("db").Value.String())
		if db {
			ota.InitDb(dbHost, dbUser, dbPass)
		}
		users, err := ota.GetUserList(idpName, -1)
		if err != nil {
			fmt.Println("Failed to get users: " + err.Error())
			return
		}

		if sortByCreationTime {
			sort.Slice(users, func(i, j int) bool {
				return users[i].CreatedDate.After(users[j].CreatedDate)
			})
		}
		usersMigrated := 0
		emailsVerified := 0
		emailsUnverified := 0
		// usersCreated := 0
		usersToPrint := []UserOutput{}
		for _, user := range users {
			if window != 0 {
				windowStartTime := time.Now().AddDate(0, 0, -window)
				if user.CreatedDate.Before(windowStartTime) {
					// Skip user
					continue
				}
			}
			if onlyUnverified && *user.EmailVerified {
				// Skip
				continue
			}

			if onlyVerified && !*user.EmailVerified {
				// Skip
				continue
			}

			if skipDomain != "" && strings.Split(user.Email, "@")[1] == skipDomain {
				// Skip
				continue
			}

			//var dbi ota.DBI = ota.MultiTenantV1DB{}
			// var userDevices []ota.Device
			// var activeDevices int
			// if db {
			// 	// Get Device Data for each user
			// 	userNamespace, _ := dbi.GetNamespaceFromDB(user.Attributes.LegacyProviderID)
			// 	userDevices, _ = ota.GetDevices(userNamespace)
			// 	activeDevices, _ = ota.GetActiveDevices(userNamespace, 30)
			// }
			// usersCreated++
			if *user.EmailVerified {
				emailsVerified++
			} else {
				emailsUnverified++
			}
			if userMigrated(user) {
				usersMigrated++
			}

			//fmt.Println("BEN SAYS: Company Name is: " + user.Attributes.CompanyName)

			usersToPrint = append(usersToPrint, UserOutput{
				FirstName:        user.FirstName,
				LastName:         user.LastName,
				Email:            user.Email,
				Company:          user.Attributes.CompanyName,
				Country:          user.Attributes.Country,
				Namespace:        user.Attributes.Namespace,
				ProviderID:       user.ID,
				LegacyProviderID: user.Attributes.LegacyProviderID,
				CreatedDate:      user.CreatedDate.Format("2006-01-02"),
				Migrated:         userMigrated(user),
			})
		}
		// Well this sucks... why didn't i just stick with the json versions of these output strings?
		attributesToPrint := []string{"First Name", "Last Name", "Email", "Company", "Country", "Created Date", "Namespace", "Provider ID", "Legacy Provider ID", "Migrated"}
		if onlyAttributes != "" {
			// Then we need to only list the specified attributes
			attributesToPrint = strings.Split(onlyAttributes, ",")
		}
		// json writer
		if outputFormat == "json" {
			allUsers := []map[string]string{}
			for _, user := range usersToPrint {
				userAttrs := map[string]string{}
				for _, attribute := range attributesToPrint {
					userAttrs[attribute] = lookupField(user, attribute)
				}
				allUsers = append(allUsers, userAttrs)
			}

			json.NewEncoder(os.Stdout).Encode(allUsers)
			// csv
		} else if outputFormat == "csv" {
			csvWriter := csv.NewWriter(os.Stdout)
			defer csvWriter.Flush()
			csvWriter.Write(attributesToPrint)
			for _, user := range usersToPrint {
				userAttributesToPrint := []string{}
				for _, attribute := range attributesToPrint {
					userAttributesToPrint = append(userAttributesToPrint, lookupField(user, attribute))
				}
				csvWriter.Write(userAttributesToPrint)
			}
			// Regular tab writer
		} else {
			writer := tabwriter.NewWriter(os.Stdout, 0, 8, 1, '\t', tabwriter.AlignRight)
			titleColumns := ""
			for _, attribute := range attributesToPrint {
				titleColumns += (attribute + "\t")
			}
			fmt.Fprintln(writer, titleColumns)

			for _, user := range usersToPrint {
				userAttributesString := ""
				for _, attribute := range attributesToPrint {
					//userAttributesToPrint := append(attributes, lookupField(user, attribute))
					userAttributesString += lookupField(user, attribute) + "\t"
				}
				fmt.Fprintln(writer, userAttributesString)
			}
			fmt.Fprintln(writer, "\t\t\t\t\t")
			fmt.Fprintln(writer, "\t\t\t\t\t")
			fmt.Fprintln(writer, "\t\t------------------\t\t\t")
			fmt.Fprintln(writer, "\t\t"+strconv.Itoa(len(usersToPrint))+" total users\t\t\t")
			fmt.Fprintln(writer, "\t\t"+strconv.Itoa(usersMigrated)+" users migrated\t\t\t")
			fmt.Fprintln(writer, "\t\t"+strconv.Itoa(emailsVerified)+" verified emails\t\t\t")
			fmt.Fprintln(writer, "\t\t"+strconv.Itoa(emailsUnverified)+" unverified emails: \t\t\t")
			writer.Flush()
		}
	},
}

var userInfoCmd = &cobra.Command{
	Use:   "info <[email] || [--providerID,p]> [--idp,i] ",
	Short: "List user information for specified email or Provider ID",
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			// See if providerID flag is set
			if providerID == "" {
				return fmt.Errorf("Email or Some flag must be set")
			}
			return nil
		}
		if saneEmail(args[0]) {
			return nil
		}
		return fmt.Errorf("Invalid input. Must specify email, or flags such as: --providerID, --all, --all-domain")
	},
	Run: func(cmd *cobra.Command, args []string) {
		var user *ota.IDPUserInfo
		var err error
		if providerID != "" {
			fmt.Println("== Getting info for user with sub|username|providerID: " + providerID)
			user, err = ota.GetUser(idpName, providerID)
			if err != nil {
				fmt.Println("Failed to get user information fromidpName Error: " + err.Error())
				return
			}
		} else {
			user, err = ota.GetUserByEmail(idpName, args[0])
			if err != nil {
				fmt.Println("Failed to get user information from" + idpName + " Error: " + err.Error())
				return
			}
		}

		config, _, err := ota.GetCurrentConfig("keycloak")
		if err != nil {
			fmt.Println("Failed to open idp config file. Error: " + err.Error())
			return
		}

		otaUsersRealmIDP := &idp.KeyCloak{}
		otaUsersRealmIDP.Init(*config)

		roles, err := otaUsersRealmIDP.GetUserRoles(user.Email)
		if err != nil {
			fmt.Println("Failed to fetch roles for user: " + user.Email + " error: " + err.Error())
			return
		}
		clientRoles, err := otaUsersRealmIDP.GetUserClientRoles(user.Email, "ota-user-manager")
		if err != nil {
			fmt.Println("Failed to fetch roles for user: " + user.Email + " error: " + err.Error())
			return
		}

		user.ClientRoles = make(map[string][]string)
		for _, role := range append(roles, clientRoles...) {
			if role.Client != "" {
				user.ClientRoles[role.Client] = append(user.ClientRoles[role.Client], role.Name)
			} else {
				user.RealmRoles = append(user.RealmRoles, role.Name)
			}
		}

		ota.PrintUserInfo(user)

		if db {
			// Get Device Data for each user
			dbi := ota.MultiTenantV1DB{}
			userNamespace, _ := dbi.GetNamespaceFromDB(user.Attributes.LegacyProviderID)
			userDevices, _ := ota.GetDevices(userNamespace)
			fmt.Println(user.Username)
			fmt.Println("Devices Provisioned: " + strconv.Itoa(len(userDevices)))
		}
	},
}

func saneEmail(email string) bool {
	items := strings.Split(email, "@")
	if len(items) == 2 {
		return true
	}
	return false
}

func createBackendUser(providerID string) error {
	// Create user in torizon backend
	client := &http.Client{}
	req, err := http.NewRequest(http.MethodPost, "http://accounts/users", nil)
	req.Header.Add("provider-id", providerID)

	if err != nil {
		return errors.New("Failed to build http request + " + err.Error())
	}

	resp, err := client.Do(req)
	// Non nil errors means the http request didn't get off the ground. It doesn't mean non 2XX
	if err != nil {
		return errors.New("Failed to get http request off the ground " + err.Error())
	}

	respBuf := bytes.Buffer{}
	respBuf.ReadFrom(resp.Body)
	resp.Body.Close()

	fmt.Println("Response from creating new user in the backend")
	fmt.Println(resp.Status)
	fmt.Println(string(respBuf.Bytes()[:]))

	if resp.StatusCode != 200 {
		if resp.StatusCode == 304 {
			fmt.Println("User already existed for user: " + providerID + " Ok")
			return nil
		}
		return errors.New("Non-200 response: Got status: " + resp.Status + " body: " + string(respBuf.Bytes()[:]))
	}
	return nil
}

// func AddDelegationRefs(namespace string, delegationReq *tuf.DelegationRequest) error {
// 	env.Config.ReposerverSvcUrl = "http://127.0.0.1:8001/api/v1/default/services/tuf-reposerver:80/proxy/"
// 	env.Config.NamespaceHeaderKey = "x-ats-namespace"
// 	toradexDelegationFile, err := tuf.GetToradexPublicDelegations()
// 	if err != nil {
// 		return errors.New("Failed to retrieve toradex public delegations file")
// 	}

// 	return tuf.AddDelegationReferences(delegationReq, &models.User{UserNamespace: namespace})

// 	resp, err := http.Get("https://artifacts.toradex.com:443/artifactory/ota-metadata-prod-frankfurt/toradex-delegations.json")
// 	if err != nil {
// 		fmt.Println("Failed to get toradex delegations :/")
// 		return err
// 	}

// 	body, err := ioutil.ReadAll(resp.Body)
// 	resp.Body.Close()

// 	reqBody := &bytes.Buffer{}
// 	writer := multipart.NewWriter(reqBody)

// 	part, _ := writer.CreateFormFile("file", "somefile")
// 	part.Write(body)

// 	writer.Close()

// 	client := &http.Client{}
// 	req, err := http.NewRequest(http.MethodPut, "http://metadata"+
// 		"/targets/delegationrefs", reqBody)
// 	req.Header.Add("provider-id", providerID)
// 	req.Header.Set("Content-Type", writer.FormDataContentType())
// 	req.Header.Set("Content-Filename", "file")

// 	if err != nil {
// 		fmt.Println("Failed to build http request + " + err.Error())
// 		return err
// 	}

// 	resp, err = client.Do(req)
// 	// Non nil errors means the http request didn't get off the ground. It doesn't mean non 2XX
// 	if err != nil {
// 		fmt.Println("Failed to get http request off the ground " + err.Error())
// 		return err
// 	}

// 	respBuf := bytes.Buffer{}
// 	respBuf.ReadFrom(resp.Body)
// 	resp.Body.Close()

// 	// fmt.Println("Response from Adding delegation references")
// 	// fmt.Println("Got response of: " + resp.Status)
// 	// fmt.Println(string(respBuf.Bytes()[:]))

// 	if resp.StatusCode != 200 {
// 		return errors.New("Failed adding delegation refs. Got http err: " + resp.Status + "\n Body: " + string(respBuf.Bytes()[:]))
// 	}
// 	return nil
// }

var userCreateCmd = &cobra.Command{
	Use:   "create [email] || [--providerID,p] || [--all] || [--all-domain <domain>] [-u <dev|prod>] ",
	Short: "Create user in TorizonOTA system",
	Long: `Create user in TorizonOTA system using an existing providerID or new email address.
	Command can create all users in cognito by using the --all flag.
	Command can create all users with a specific domain by specifying the --all-domain <domain>.
	   Examples would be for an email address of a company. Like --all-domain toradex.com`,
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			// See if providerID flag is set
			if (providerID == "") && (allFlag == false) && (allDomainFlag == "") {
				return fmt.Errorf("Email or Some flag must be set")
			}
			return nil
		}
		if saneEmail(args[0]) {
			return nil
		}
		return fmt.Errorf("Invalid input. Must specify email, or flags such as: --providerID, --all, --all-domain")
	},
	Run: func(cmd *cobra.Command, args []string) {

		if providerID != "" {
			fmt.Println("== Processing user creation for cognito user with sub|username|providerID: " + providerID)
			_, err := ota.GetUser(idpName, providerID)
			if err != nil {
				fmt.Println("Failed to get user information from cognito, valid provider id???")
				return
			}
			if err := createBackendUser(providerID); err != nil {
				fmt.Println("Failed to create user in backend")
				fmt.Println(err.Error())
				return
			}
		} else if allFlag {
			fmt.Println("== Processing user creation for ALL existing cognito users")
			users, err := ota.GetUserList(idpName, -1)
			if err != nil {
				fmt.Println(err.Error())
				return
			}
			if len(users) == 0 {
				fmt.Println("No users returned from cognito")
				return
			}

			for _, user := range users {
				fmt.Println("Creating user: " + user.Email + " (" + user.Attributes.LegacyProviderID + ")...")
				if err := createBackendUser(user.Attributes.LegacyProviderID); err != nil {
					fmt.Println("Failed to create backend user for: " + user.Email + " (" + user.Attributes.LegacyProviderID + ")")
					fmt.Println(err.Error())
					return
				}
			}
		} else if allDomainFlag != "" {
			fmt.Println("== Processing user creation for ALL cognito users with matching email domain " + allDomainFlag)
			users, err := ota.GetUserList(idpName, -1)
			if err != nil {
				fmt.Println(err.Error())
				return
			}
			if len(users) == 0 {
				fmt.Println("No users returned from cognito")
				return
			}

			for _, user := range users {
				// In case you forgot what emails look like:
				// ben.clouser@toradex.com => ["ben.clouser", "toradex.com"]
				emailPieces := strings.Split(user.Email, "@")
				if len(emailPieces) < 2 {
					fmt.Println("Invalid email registered with user: " + user.Email + "(" + user.Attributes.LegacyProviderID + "). Skipping")
					continue
				}
				if emailPieces[1] == allDomainFlag {
					// Run create-user routine
					fmt.Println("Creating user: " + user.Email + " (" + user.Attributes.LegacyProviderID + ")...")
					if err := createBackendUser(user.Attributes.LegacyProviderID); err != nil {
						fmt.Println("Failed to create backend user for: " + user.Email + " (" + user.Attributes.LegacyProviderID + ")")
						fmt.Println(err.Error())
						return
					}
				}
			}
		} else if len(args) > 0 {
			// Full-blown user creation
			fmt.Println("User creation including cognito step. Not Yet implemented")
			return
		}
		fmt.Println("All done")
		return
	},
}

var userDeleteCmd = &cobra.Command{
	Use:   "delete [email] || [--providerID] || [--all] || [--all-domain <domain>] [--userPool,-u <dev|prod>] ",
	Short: "Delete user in TorizonOTA system",
	Long: `Delete user in TorizonOTA system using an existing providerID or email
	Command can delete all users in the system by using the --all flag.
	Command can delete all users with a specific domain by specifying the --all-domain <domain>.
	   Useful for deletion of all email addresses of a company. Like --all-domain toradex.com`,
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			// See if providerID flag is set
			if (providerID == "") && (allFlag == false) && (allDomainFlag == "") {
				return fmt.Errorf("Email or Some flag must be set")
			}
			return nil
		}
		if saneEmail(args[0]) {
			return nil
		}
		return fmt.Errorf("Invalid input. Must specify email, or flags such as: --providerID, --all, --all-domain")
	},
	Run: func(cmd *cobra.Command, args []string) {

		if providerID != "" {
			fmt.Println("== Processing user deletion for cognito user with sub|username|providerID: " + providerID)
			_, err := ota.GetUser(idpName, providerID)
			if err != nil {
				fmt.Println("Failed to get user information from cognito, valid provider id???")
				return
			}
			if err := deleteUser(providerID); err != nil {
				fmt.Println("Failed to delete user (" + providerID + ") in backend")
				fmt.Println(err.Error())
				return
			}
		} else if allFlag {
			fmt.Println("== Processing user deletion for ALL existing cognito users")
			users, err := ota.GetUserList(idpName, -1)
			if err != nil {
				fmt.Println(err.Error())
				return
			}
			if len(users) == 0 {
				fmt.Println("No users returned from cognito")
				return
			}

			for _, user := range users {
				fmt.Println("Deleting user: " + user.Email + " (" + user.Attributes.LegacyProviderID + ")...")
				if err := deleteUser(user.Attributes.LegacyProviderID); err != nil {
					fmt.Println("Failed to create backend user for: " + user.Email + " (" + user.Attributes.LegacyProviderID + ")")
					fmt.Println(err.Error())
					return
				}
			}
		} else if allDomainFlag != "" {
			fmt.Println("== Processing user deletion for ALL cognito users with matching email domain " + allDomainFlag)
			users, err := ota.GetUserList(idpName, -1)
			if err != nil {
				fmt.Println(err.Error())
				return
			}
			if len(users) == 0 {
				fmt.Println("No users returned from cognito")
				return
			}

			for _, user := range users {
				// In case you forgot what emails look like:
				// ben.clouser@toradex.com => ["ben.clouser", "toradex.com"]
				emailPieces := strings.Split(user.Email, "@")
				if len(emailPieces) < 2 {
					fmt.Println("Invalid email registered with user: " + user.Email + "(" + user.Attributes.LegacyProviderID + "). Skipping")
					continue
				}
				if emailPieces[1] == allDomainFlag {
					// Run delete-user routine
					fmt.Println("Deleting user: " + user.Email + " (" + user.Attributes.LegacyProviderID + ")...")
					if err := deleteUser(user.Attributes.LegacyProviderID); err != nil {
						fmt.Println("Failed to delete user for: " + user.Email + " (" + user.Attributes.LegacyProviderID + ")")
						fmt.Println(err.Error())
						return
					}
				}
			}
		}
		fmt.Println("All done")
		return
	},
}

var userMigrateToKeycloakCmd = &cobra.Command{
	Use:   "migrate <[email] || [--providerID,p] || [--all]>",
	Short: "Migrate user(s) from cognito to keycloak identity provider",
	Run: func(cmd *cobra.Command, args []string) {
		users := []*ota.IDPUserInfo{}
		if len(args) < 1 {
			// See if providerID flag is set
			if (providerID == "") && (allFlag == false) {
				fmt.Errorf("Email must be specified, or --providerID, or --all flag must be set")
				return
			}
			if providerID != "" {
				user, err := ota.GetUser(cognitoIDPName, providerID)
				if err != nil {
					fmt.Println("Failed to get user information from cognito. Error: " + err.Error())
					return
				}
				users = append(users, user)
			} else if allFlag {
				usersList, err := ota.GetUserList(cognitoIDPName, -1)
				if err != nil {
					fmt.Println("Failed to get users from cognito: " + err.Error())
					return
				}
				users = usersList
			}
		} else {
			if !saneEmail(args[0]) {
				fmt.Println("Invalid email specified")
				return
			}
			// lookup user via email
			userFromEmail, err := ota.GetUserByEmail(cognitoIDPName, args[0])
			users = append(users, userFromEmail)
			if err != nil {
				fmt.Println("Failed to get user by email: " + err.Error())
				return
			}
		}

		db, _ := strconv.ParseBool(cmd.Flags().Lookup("db").Value.String())
		if db {
			fmt.Println("db specified... performing db operations")
			ota.InitDb(dbHost, dbUser, dbPass)
		}
		fmt.Println("Total number of users to migrate: ", len(users))
		fmt.Println("")
		fmt.Println("Do you want to start migration? (y/n)?")
		var yesNo string
		fmt.Scanln(&yesNo)
		if yesNo == "n" {
			fmt.Println("Ok, Stopping here.")
			return
		}

		for _, user := range users {
			// only migrate a user's namespace. Useful for synchronizing after a full migration
			if onlyNamespace == true {
				if db != true {
					fmt.Println("db must be set to true when migrating a user's namespace")
					return
				}
				kcUser, err := ota.GetUser(keycloakIDPName, user.Email)
				if err != nil {
					fmt.Println("Failed to get user from keycloak Error: " + err.Error())
					if !userWantsToContinue() {
						return
					}
					continue
				}

				dbi := ota.MultiTenantV2DB{}
				namespace, err := dbi.GetNamespaceFromDB(user.Attributes.LegacyProviderID)
				if (err != nil) || (namespace == "") {
					fmt.Println("Failed to lookup user (" + user.Email + ")'s namespace in db using providerID: " + user.Attributes.LegacyProviderID)
					if err != nil {
						fmt.Println("Error: " + err.Error())
					}
					if !userWantsToContinue() {
						return
					}
					continue
				}
				if namespace != kcUser.Attributes.Namespace {
					if overwrite {
						fmt.Println("Overwrite flag is set so we will overwrite namespace in keycloak with db value: " + namespace)
					} else {
						// ask user
						fmt.Println("Namespace in db: " + namespace + ", namespace in keycloak: " + kcUser.Attributes.Namespace)
						fmt.Println("Update keycloak with the namespce from db? (y/n)")
						var yesNo string
						fmt.Scanln(&yesNo)
						if yesNo == "n" {
							fmt.Println("Ok, moving onto next user")
							continue
						}
					}
					// Assumes user exists in keycloak already
					fmt.Println("Updating namespace in keycloak for user: " + user.Email + ", to namespace: " + namespace)
					err = ota.UpdateUserNamespace(keycloakIDPName, user.Email, namespace)
					if err != nil {
						fmt.Println("Failed to set user's (" + user.Email + ") namespace in keycloak. Error:  " + err.Error())
						if !userWantsToContinue() {
							return
						}
						continue
					}
				} else {
					fmt.Println("Namespace in keycloak matches namespace db: " + user.Email + ", namespace: " + namespace)
					fmt.Println("no need to update...")
				}
			} else {
				// normal migration
				fmt.Println("Migrating user: " + user.Email + " ...")
				// Since we are migrating we need to set the migration flag
				user.RequiredActions = append(user.RequiredActions, "UPDATE_PROFILE")
				err := ota.CreateUser(keycloakIDPName, *user)
				if err != nil {
					fmt.Println("Failed to create user in keycloak: " + user.Email)
					fmt.Println("Error: " + err.Error())
					if !userWantsToContinue() {
						return
					}
				}
			}
		}
	},
}

func deleteUser(providerID string) error {
	if err := ota.DeleteBackendUser(providerID); err != nil {
		fmt.Println("Failed to delete user from backend: " + providerID + " " + err.Error())
		return err
	}

	if err := ota.DeleteCognitoUser(providerID, userPool); err != nil {
		fmt.Println("Failed to delete user from cognito: " + providerID + " " + err.Error())
		return err
	}
	return nil
}

var updateUserCmd = &cobra.Command{
	Use:   "update <email> [--enabled true|false]",
	Short: "Update a user in the identity provider",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 || args[0] == "" {
			fmt.Println("Must specify email address as argument 1")
			return
		}
		email := args[0]
		config, _, err := ota.GetCurrentConfig("keycloak")
		if err != nil {
			fmt.Println("Failed to open idp config file. Error: " + err.Error())
			return
		}
		idpClient := idp.New(idpName, *config)
		// Lookup userID from the email
		user, err := idpClient.GetUser(email)
		if err != nil {
			fmt.Println("Failed to lookup user via email! " + email + ". Error: " + err.Error())
			return
		}
		falseBool := false
		trueBool := true
		somethingToUpdate := false
		if enabled == "false" {
			user.Enabled = &falseBool
			somethingToUpdate = true
		} else if enabled == "true" {
			user.Enabled = &trueBool
			somethingToUpdate = true
		}

		if attributes != "" {
			attributeKeyValues := strings.Split(attributes, ",")
			for _, attributeKeyValue := range attributeKeyValues {
				keysValues := strings.Split(attributeKeyValue, "=")
				if len(keysValues)%2 != 0 {
					fmt.Println("Mismatched number of attribute keys to values. Failed to parse attributes and values from input string")
				}
				for i := 0; i <= len(keysValues)/2; i += 2 {
					switch keysValues[i] {
					case "namespace":
						fmt.Println("ok, setting namespace to " + keysValues[i+1])
						user.Attributes.Namespace = keysValues[i+1]
						somethingToUpdate = true
					case "legacy_provider_id":
						fmt.Println("ok, setting legacyProviderID to " + keysValues[i+1])
						user.Attributes.LegacyProviderID = keysValues[i+1]
						somethingToUpdate = true
					default:
						fmt.Println("Unknown attribute key specified: " + keysValues[i])
					}
				}
			}
		}

		if !somethingToUpdate {
			fmt.Println("Nothing specified to update")
			return
		}
		// We will need the providerID
		err = idpClient.UpdateUser(email, *user)
		if err != nil {
			fmt.Println("Failed to update user. Error: " + err.Error())
		}
	},
}

func upsertUserMetadata(user *models.User, requestData map[string]interface{}, dbHost string) (map[string]string, error) {
	env.Config.DBHost = dbHost
	db := dao.GetDbHandle("user_registry")
	db.Model(&models.Metadata{}).ModifyColumn("fieldValue", "longtext")
	metaDao := new(dao.MetadataDao)
	var upserted map[string]string
	upserted, err := metaDao.UpsertMetaArray(user.UserNamespace, requestData)
	if err != nil {
		return nil, errors.New("Failed to upsertMetaArray. Error: " + err.Error())
	}
	return upserted, nil
}

var metadataCmd = &cobra.Command{
	Use:   "metadata <email> <set <key> <value>|get> <--dbHost|-d>",
	Short: "Get a user's metadata from the backend",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 || args[0] == "" {
			fmt.Println("Must specify email address as argument 1")
			return
		}
		if len(args) == 1 || args[1] == "" {
			fmt.Println("Must specify set or get as argument 2")
			return
		}
		if len(args) >= 2 && (args[1] != "set" && args[1] != "get") {
			fmt.Println("Must specify set or get as argument 2")
			return
		}
		var metadataKey = ""
		var metadataValue = ""
		if args[1] == "set" {
			if len(args) != 4 {
				fmt.Println("Must specify key and value as arguments 3 and 4")
				return
			}
			metadataKey = args[2]
			metadataValue = args[3]
		}
		if dbHost == "" {
			fmt.Println("Must specify dbHost")
			return
		}
		email := args[0]
		config, _, err := ota.GetCurrentConfig("keycloak")
		if err != nil {
			fmt.Println("Failed to open idp config file. Error: " + err.Error())
			return
		}
		idpClient := idp.New(idpName, *config)
		// Lookup userID from the email
		user, err := idpClient.GetUser(email)
		if err != nil {
			fmt.Println("Failed to lookup user via email! " + email + ". Error: " + err.Error())
			return
		}

		env.Config.DBHost = dbHost
		ui := userInterface.New(&userInterface.UserDBActions{FetchMetas: true}, &userInterface.UserExtActions{})
		someUser, err := ui.Get(user.Attributes.Namespace)
		if err != nil {
			fmt.Println("Failed to find user for namespace (" + user.Attributes.Namespace + ") in database. Error: " + err.Error())
			return
		}

		// set metadata
		if metadataKey != "" {
			metadataToUpsert := map[string]interface{}{metadataKey: metadataValue}
			if valueBool {
				value, err := strconv.ParseBool(metadataValue)
				if err != nil {
					fmt.Println("Failed to parse incoming value as a boolean!. Error: " + err.Error())
					return
				}
				metadataToUpsert = map[string]interface{}{metadataKey: value}
			}

			metadata, err := upsertUserMetadata(someUser, metadataToUpsert, dbHost)
			if err != nil {
				fmt.Println("Failed to set metadata. Error: " + err.Error())
				return
			}
			fmt.Println(metadata)
		} else {
			someBytes, err := json.Marshal(someUser.Meta)
			if err != nil {
				fmt.Println("Failed to marshal user metadata into json. Error: " + err.Error())
				return
			}
			fmt.Println(string(someBytes[:]))
		}
	},
}

var groupsCmd = &cobra.Command{
	Use:   "groups members <groupName>",
	Short: "Get user's that belong to specified group",
	Run: func(cmd *cobra.Command, args []string) {

		if len(args) == 0 || args[0] == "" {
			fmt.Println("Must specify members as argument 1")
			return
		}
		if len(args) > 0 && args[0] != "members" {
			fmt.Println("Must specify members as argument 1")
			return
		}
		if len(args) < 2 {
			fmt.Println("Must specify groupName as argument 2")
			return
		}
		groupId := args[1]

		config, _, err := ota.GetCurrentConfig("keycloak")
		if err != nil {
			fmt.Println("Failed to open idp config file. Error: " + err.Error())
			return
		}

		otaUsersRealmIDP := &idp.KeyCloak{}
		otaUsersRealmIDP.Init(*config)

		// Lookup userID from the email
		users, err := otaUsersRealmIDP.GetGroupMembers(groupId)
		if err != nil {
			fmt.Println("Failed to lookup groupId " + groupId + ". Error: " + err.Error())
			return
		}

		someBytes, err := json.Marshal(users)
		if err != nil {
			fmt.Println("Failed to marshal user members into json. Error: " + err.Error())
			return
		}
		fmt.Println(string(someBytes[:]))
	},
}

// var rolesCmd = &cobra.Command{
// 	Use:   "roles <email>",
// 	Short: "Get roles that belong to a user",
// 	Run: func(cmd *cobra.Command, args []string) {
// 		fmt.Println(args)
// 		if len(args) == 0 || args[0] == "" {
// 			fmt.Println("Must specify group ID as argument 1")
// 			return
// 		}
// 		groupId := args[0]

// 		config, _, err := ota.GetCurrentConfig("keycloak")
// 		if err != nil {
// 			fmt.Println("Failed to open idp config file. Error: " + err.Error())
// 			return
// 		}

// 		otaUsersRealmIDP := &idp.KeyCloak{}
// 		otaUsersRealmIDP.Init(*config)

// 		// Lookup userID from the email
// 		users, err := otaUsersRealmIDP.GetGroupMembers(groupId)
// 		if err != nil {
// 			fmt.Println("Failed to lookup groupId " + groupId + ". Error: " + err.Error())
// 			return
// 		}

// 		someBytes, err := json.Marshal(users)
// 		if err != nil {
// 			fmt.Println("Failed to marshal user members into json. Error: " + err.Error())
// 			return
// 		}
// 		fmt.Println(string(someBytes[:]))
// 	},
// }

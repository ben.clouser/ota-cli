package cmd

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/ben.clouser/ota-cli/ota"
	"gitlab.com/torizon-platform/idp"
	"gitlab.com/torizon-platform/sdk/env"
	"gitlab.com/torizon-platform/sdk/models"
	"gitlab.com/torizon-platform/sdk/tuf"
)

func init() {
	rootCmd.AddCommand(delegationCmd)
	delegationCmd.PersistentFlags().StringVarP(&idpName, "idp", "i", keycloakIDPName, "Identity Provider to use for command. Defaults to "+keycloakIDPName)
	delegationCmd.AddCommand(delegationListCmd)
	delegationCmd.AddCommand(addDelegationRefsCmd)
	addDelegationRefsCmd.Flags().StringVarP(&providerID, "providerID", "p", "", "providerID - Assumes user already exists in provider")
	addDelegationRefsCmd.Flags().BoolVarP(&allFlag, "all", "", false, "All users in identity provider")
	addDelegationRefsCmd.Flags().StringVarP(&allDomainFlag, "all-domain", "", "", "All users matching specified domain from list of users in provider")
	delegationCmd.AddCommand(syncDelegationMetadata)
	syncDelegationMetadata.Flags().StringVarP(&providerID, "providerID", "p", "", "providerID - Assumes user already exists in provider")
	syncDelegationMetadata.Flags().BoolVarP(&allFlag, "all", "", false, "All users in identity provider")
	syncDelegationMetadata.Flags().StringVarP(&allDomainFlag, "all-domain", "", "", "All users matching specified domain from list of users in provider")
	delegationCmd.AddCommand(setRemoteDelegation)
	setRemoteDelegation.Flags().StringVarP(&providerID, "providerID", "p", "", "providerID - Assumes user already exists in provider")
	setRemoteDelegation.Flags().BoolVarP(&allFlag, "all", "", false, "All users in identity provider")
	setRemoteDelegation.Flags().StringVarP(&allDomainFlag, "all-domain", "", "", "All users matching specified domain from list of users in provider")
	setRemoteDelegation.Flags().StringVarP(&remoteHeaders, "headers", "", "", "csv list of key:value pairs. ie. key:value,key2:value2,key3:value3")
}

var delegationCmd = &cobra.Command{
	Use:   "delegation",
	Short: "Commands pertaining to a user's delegated metadata",
}

var delegationListCmd = &cobra.Command{
	Use:   "ls <providerId>",
	Short: "List delegations in a user's repository. REQUIRES ACTIVE KUBERNETES PROXY",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 {
			fmt.Println("User provider id must be provided as argument 1")
			return
		}
		idpUser, err := ota.GetUser(idpName, args[0])
		if err != nil {
			fmt.Println("Failed to get user information from provider, Error: " + err.Error())
			return
		}
		env.Config.ReposerverSvcUrl = "127.0.0.1:8001/api/v1/namespaces/default/services/tuf-reposerver:80/proxy"
		env.Config.NamespaceHeaderKey = "x-ats-namespace"
		delegations, err := tuf.GetDelegationsInfo(&models.User{UserNamespace: idpUser.Attributes.Namespace})
		if err != nil {
			fmt.Println("Failed to get delegations info. Error: " + err.Error())
			return
		}
		out, err := json.Marshal(delegations)
		fmt.Println(string(out[:]))
	},
}

func GetToradexPublicDelegations() (tuf.DelegationRequest, error) {
	delegationRequest := tuf.DelegationRequest{
		DelegationKeys: []tuf.DelegationKeyDesc{
			{
				KeyName: "tdx-online",
				KeyID:   "a6aeb9734c5dacafa72a70579cc360aa9c4647215da8ae431a463d2097910343",
				PubKey: tuf.DelegationPubKey{
					KeyVal:  tuf.DelegationPubKeyVal{Public: "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxVSI7kFSR1tYzWroi5VY\nVXUlFjk9Zj2wjXUAXag3KpomOgvVU4txp2iD5fcy7oRw55B/IVvHfLgqGF6FJ5Jm\nu8S6HPVvkx866SMWC9pyEiXQTRBrwzu+iZjp95hbwuk3NWwKLm1lMn7kuQ9SuGRC\nUbbO5CSSTqZw3htW7Qfs0O1LtQHzPG3bZIABruUNCGpSB3azDj5pVUYVA7yV6FP8\n+4JNQFi5DFY5mUmmqDBM66yb6XlSB1r4Z8WyEELvWH8xG02Sagfs3OKPkwKpf55F\nrI/K5S95TGoCV9x+QBXZ0kwrCHO2AzBVOn4/AoZ5+yruPLgU5NZ6ZrkoI6l5YJo0\nnwIDAQAB\n-----END PUBLIC KEY-----\n"},
					KeyType: "RSA",
				},
			},
			{
				KeyName: "tdx-offline",
				KeyID:   "75268c4c167d3cab5edcfdf55474be4e5a3cee9932546395184ce2461a33ffc9",
				PubKey: tuf.DelegationPubKey{
					KeyVal:  tuf.DelegationPubKeyVal{Public: "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA3XJVflolvhgwCqMgai3l\n01I2awDUlGYkeiFR6L1T2XyLxZZ0VeOBm6awgYTRMLxpRAjHBqlX6oIgcmvBkx1D\nlS7mYMjauLoeWEP/NnFZlh1UVLyqNCdnNKMwrpFqFlWIl3FVc9S0xy+aRH120YTx\nqmKxZ9bi0bM98v6a/h4BAZiD6jzyxpr7HxB4NmCDQxQ8a4F2SOAaIoNYbFXTyAjL\n/FFCeHoLyElEiVeQq+k6//+q/8gcmwED2f3PhXbAzwrMw/6bqajmMTm/2dP+KJ8Y\nFgEmgah4FSdnGJ2qAgpTBFS0on9m/qqDTqnaIZS/1V51ytff71EIPAXjF+QFPu+K\nFQIDAQAB\n-----END PUBLIC KEY-----\n"},
					KeyType: "RSA",
				},
			},
		},
		Delegations: []tuf.DelegationReference{
			{
				Name:   "tdx-nightly",
				Prefix: "*/nightly-*",
				Keys: []string{
					"tdx-online",
					"tdx-offline",
				},
			},
			{
				Name:   "tdx-monthly",
				Prefix: "*/monthly-*",
				Keys: []string{
					"tdx-online",
					"tdx-offline",
				},
			},
			{
				Name:   "tdx-quarterly",
				Prefix: "*/release-*",
				Keys: []string{
					"tdx-online",
					"tdx-offline",
				},
			},
			{
				Name:   "tdx-lts",
				Prefix: "*/release-*",
				Keys: []string{
					"tdx-online",
					"tdx-offline",
				},
			},
			{
				Name:   "tdx-containers",
				Prefix: "*",
				Keys: []string{
					"tdx-online",
					"tdx-offline",
				},
			},
		},
	}
	return delegationRequest, nil
}

var addDelegationRefsCmd = &cobra.Command{
	Use:   "delegationrefs [--providerID,p] || [--all] || [--all-domain <domain>] [-u <dev|prod>]",
	Short: "(re) add Toradex delegations to a user's targets metadata in TorizonOTA system",
	Long: `Assumes a kubernetes api proxy is hosted on 127.0.0.1:8001 ! 
	Add or update Toradex delegation references in a user's targets metadata
	Command can add delegation refs for all users in keycloak by using the --all flag.
	Command can add delegation refs all users with a specific domain by specifying the --all-domain <domain>.
	   An example: would be for an email address of a company. Like --all-domain toradex.com`,
	Run: func(cmd *cobra.Command, args []string) {
		env.Config.ReposerverSvcUrl = "127.0.0.1:8001/api/v1/namespaces/default/services/tuf-reposerver:80/proxy"
		env.Config.NamespaceHeaderKey = "x-ats-namespace"
		if providerID != "" {
			fmt.Println("== Processing user delegation ref add for cognito user with sub|username|providerID: " + providerID)
			idpUser, err := ota.GetUser(idpName, providerID)
			if err != nil {
				fmt.Println("Failed to get user information from provider, Error: " + err.Error())
				return
			}
			toradexDelegationFile, err := GetToradexPublicDelegations()
			if err != nil {
				fmt.Println("Failed to get Toradex public delegations file")
			}
			if err := tuf.AddDelegationReferences(&toradexDelegationFile, &models.User{UserNamespace: idpUser.Attributes.Namespace}); err != nil {
				fmt.Println("Failed to add delegation references")
				fmt.Println(err.Error())
				return
			}
		} else if allFlag {
			fmt.Println("== Processing user delegation ref add for ALL existing cognito users")

			users, err := ota.GetUserList(idpName, -1)
			if err != nil {
				fmt.Println(err.Error())
				return
			}
			if len(users) == 0 {
				fmt.Println("No users returned from provider")
				return
			}
			toradexDelegationFile, err := GetToradexPublicDelegations()
			if err != nil {
				fmt.Println("Failed to get Toradex public delegations file")
			}
			for _, user := range users {
				fmt.Println("Adding delegation refs to user: " + user.Email + " providerID: " + user.ID + "...")
				if err := tuf.AddDelegationReferences(&toradexDelegationFile, &models.User{UserNamespace: user.Attributes.Namespace}); err != nil {
					fmt.Println("Failed to add delegation references")
					fmt.Println(err.Error())
					return
				}
			}
		} else if allDomainFlag != "" {
			fmt.Println("== Processing user delegation ref add for ALL provider users with matching email domain " + allDomainFlag)

			users, err := ota.GetUserList(idpName, -1)
			if err != nil {
				fmt.Println(err.Error())
				return
			}
			if len(users) == 0 {
				fmt.Println("No users returned from provider")
				return
			}
			toradexDelegationFile, err := GetToradexPublicDelegations()
			for _, user := range users {
				// In case you forgot what emails look like:
				// ben.clouser@toradex.com => ["ben.clouser", "toradex.com"]
				emailPieces := strings.Split(user.Email, "@")
				if len(emailPieces) < 2 {
					fmt.Println("Invalid email registered with user: " + user.Email + "(" + user.Attributes.LegacyProviderID + "). Skipping")
					continue
				}
				if emailPieces[1] == allDomainFlag {
					// Run Add delegation refs routine
					fmt.Println("Adding delegation refs to user: " + user.Email + " providerID: " + user.ID + "...")
					if err := tuf.AddDelegationReferences(&toradexDelegationFile, &models.User{UserNamespace: user.Attributes.Namespace}); err != nil {
						fmt.Println("Failed to add delegation references")
						fmt.Println(err.Error())
						return
					}
				}
			}
		}
	},
}

// delete delegation ref command
// curl --verbose -H "x-ats-namespace: faf03b69-1739-4b20-98bc-b20274311549" http://tuf-reposerver/api/v1/user_repo/trusted-delegations | jq .

var syncDelegationMetadata = &cobra.Command{
	Use:   "delegationsync [--providerID,p] || [--all] || [--all-domain <domain>]",
	Short: "Sync Toradex delegation metadata into a users target repository in a TorizonOTA system",
	Long: `Assumes a kubernetes api proxy is hosted on 127.0.0.1:8001 ! 
	Sync a user(s) delegation metadata with Toradex's delegation metadata. This will refresh the list of
	targets that a user has available in their targets repository
	Command can add delegation refs for all users in keycloak by using the --all flag.
	Command can add delegation refs all users with a specific domain by specifying --all-domain <domain>.
	   An example: would be for an email address of a company. Like --all-domain toradex.com`,
	Run: func(cmd *cobra.Command, args []string) {
		env.Config.ReposerverSvcUrl = "127.0.0.1:8001/api/v1/namespaces/default/services/tuf-reposerver:80/proxy"
		env.Config.NamespaceHeaderKey = "x-ats-namespace"
		usersToProcess := []*idp.User{}
		if providerID != "" {
			fmt.Println("Processing user delegation sync for providerID: " + providerID)
			idpUser, err := ota.GetUser(keycloakIDPName, providerID)
			if err != nil {
				fmt.Println("Failed to get user information from provider, Error: " + err.Error())
				return
			}
			usersToProcess = append(usersToProcess, idpUser)
		} else if allFlag {
			fmt.Println("Processing user delegation sync for ALL existing keycloak users")

			users, err := ota.GetUserList(keycloakIDPName, -1)
			if err != nil {
				fmt.Println(err.Error())
				return
			}
			if len(users) == 0 {
				fmt.Println("No users returned from provider")
				return
			}
			usersToProcess = users
		} else if allDomainFlag != "" {
			fmt.Println("Processing user delegation sync for ALL provider users with matching email domain " + allDomainFlag)

			users, err := ota.GetUserList(idpName, -1)
			if err != nil {
				fmt.Println(err.Error())
				return
			}
			if len(users) == 0 {
				fmt.Println("No users returned from provider")
				return
			}
			for _, kcUser := range users {
				// In case you forgot what emails look like:
				// ben.clouser@toradex.com => ["ben.clouser", "toradex.com"]
				emailPieces := strings.Split(kcUser.Email, "@")
				if len(emailPieces) < 2 {
					fmt.Println("Invalid email registered with user: " + kcUser.Email + "(" + kcUser.Attributes.LegacyProviderID + "). Skipping")
					continue
				}
				if emailPieces[1] == allDomainFlag {
					usersToProcess = append(usersToProcess, kcUser)
				}
			}
		}
		// Get the toradex delegation metadata for all delegation roles (nightly, monthly, etc.)
		// Sneaky manual struct creation to avoid the db hits
		defaultUser := models.User{
			UserNamespace: "default",
		}
		tdxDelegationMeta := map[tuf.DelegatedRole]string{}
		for _, delegatedRole := range tuf.DelegatedRoles() {

			// get it from default user's repo
			tdxDelegatedMetadataFile, err := tuf.GetDelegatedMetadata(delegatedRole, &defaultUser)
			if err != nil {
				fmt.Println("Failed to get Delegation metadata. Error: " + err.Error())
				return
			}
			tdxDelegationMeta[delegatedRole] = tdxDelegatedMetadataFile
		}
		fmt.Println("About to process delegations sync for " + strconv.Itoa(len(usersToProcess)) + " user(s)")
		for _, kcUser := range usersToProcess {
			if kcUser.Attributes.Namespace == "" {
				fmt.Println("User (" + kcUser.Email + ") does not have a namespace assigned to it. Skipping...")
				continue
			}
			fmt.Println("User: " + kcUser.Email + ", namespace(" + kcUser.Attributes.Namespace + ")")
			for delegatedRole, delegationFile := range tdxDelegationMeta {
				fmt.Println("    Syncing: " + delegatedRole.FileName())

				// This is kind of sneaky... but to avoid a database lookup we can just fill out the user info manually using idp user info
				thisUser := models.User{
					UserNamespace: kcUser.Attributes.Namespace,
					ProviderID:    kcUser.ID,
				}
				// publish to user's repo
				if err := tuf.PublishDelegatedMetadata(delegatedRole, delegationFile, &thisUser); err != nil {
					fmt.Println("Failed to publish delegated metadata (" + delegatedRole.FileName() + "). Error: " + err.Error())
					continue
				}
			}
		}
	},
}

var setRemoteDelegation = &cobra.Command{
	Use:   "remotedelegation <delegation-name> <remote-uri> [--headers,h] ||[--providerID,p] || [--all] || [--all-domain <domain>]",
	Short: "Create a remote delegation for the specified user",
	Long: `Assumes a kubernetes api proxy is hosted on 127.0.0.1:8001 !
	Add a remote delegation to a users repository.
	Specify the delegation name, remote uri and any optional headers used to fetch the delegation from remote uri
	Optional headers are a csv list of key:value pairs. Ie: "key:value,key2:value2,key3:value3"
	Command can add delegation refs for all users in keycloak by using the --all flag.
	Command can add delegation refs all users with a specific domain by specifying --all-domain <domain>.
	   An example: would be for an email address of a company. Like --all-domain toradex.com`,
	Run: func(cmd *cobra.Command, args []string) {
		env.Config.ReposerverSvcUrl = "127.0.0.1:8001/api/v1/namespaces/default/services/tuf-reposerver:80/proxy"
		env.Config.NamespaceHeaderKey = "x-ats-namespace"
		usersToProcess := []*idp.User{}
		if len(args) < 2 {
			fmt.Println("Command requires at least two arguments: delegation name and remote-uri")
			return
		}
		delegationName := args[0]
		remoteUri := args[1]
		headers := map[string]string{}
		if remoteHeaders != "" {
			for _, headerkv := range strings.Split(remoteHeaders, ",") {
				kv := strings.Split(headerkv, ":")
				headers[kv[0]] = kv[1]
			}
		}
		if providerID != "" {
			fmt.Println("Processing remote delegation creation for providerID: " + providerID)
			idpUser, err := ota.GetUser(keycloakIDPName, providerID)
			if err != nil {
				fmt.Println("Failed to get user information from provider, Error: " + err.Error())
				return
			}
			usersToProcess = append(usersToProcess, idpUser)
		} else if allFlag {
			fmt.Println("Processing remote delegation creation for ALL existing keycloak users")

			users, err := ota.GetUserList(keycloakIDPName, -1)
			if err != nil {
				fmt.Println(err.Error())
				return
			}
			if len(users) == 0 {
				fmt.Println("No users returned from provider")
				return
			}
			usersToProcess = users
		} else if allDomainFlag != "" {
			fmt.Println("Processing user remote delegation creation for ALL provider users with matching email domain " + allDomainFlag)

			users, err := ota.GetUserList(idpName, -1)
			if err != nil {
				fmt.Println(err.Error())
				return
			}
			if len(users) == 0 {
				fmt.Println("No users returned from provider")
				return
			}
			for _, kcUser := range users {
				// In case you forgot what emails look like:
				// ben.clouser@toradex.com => ["ben.clouser", "toradex.com"]
				emailPieces := strings.Split(kcUser.Email, "@")
				if len(emailPieces) < 2 {
					fmt.Println("Invalid email registered with user: " + kcUser.Email + "(" + kcUser.Attributes.LegacyProviderID + "). Skipping")
					continue
				}
				if emailPieces[1] == allDomainFlag {
					usersToProcess = append(usersToProcess, kcUser)
				}
			}
		}

		if len(usersToProcess) != 1 {
			fmt.Println("About to create remote delegations for " + strconv.Itoa(len(usersToProcess)) + " user(s)")
		}
		fmt.Println("Delegation name: " + delegationName + ", Remote URI: " + remoteUri)
		for _, kcUser := range usersToProcess {
			if kcUser.Attributes.Namespace == "" {
				fmt.Println("User (" + kcUser.Email + ") does not have a namespace assigned to it. Skipping...")
				continue
			}
			fmt.Println("User: " + kcUser.Email + ", namespace(" + kcUser.Attributes.Namespace + ")")
			// This is kind of sneaky... but to avoid a database lookup we can just fill out the user info manually using idp user info
			thisUser := models.User{
				UserNamespace: kcUser.Attributes.Namespace,
				ProviderID:    kcUser.ID,
			}
			if err := tuf.CreateRemoteDelegation(tuf.AsDelegatedRole(delegationName), remoteUri, headers, &thisUser); err != nil {
				fmt.Println("Failed to create remote delegation for user: " + kcUser.Email + ". Error: " + err.Error())
				continue
			}
		}
	},
}

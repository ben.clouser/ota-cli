package ota

import (
	"database/sql"
	"log"
	"strconv"
	"strings"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

// global handle to the databases
var deviceRegDB *sql.DB
var userRegDB *sql.DB

func InitDb(dbHost, dbUser, dbPass string) {
	var err error
	deviceRegDB, err = sql.Open("mysql", dbUser+":"+dbPass+"@tcp("+dbHost+":3306)/device_registry")
	if err != nil {
		log.Panic("Failed to open device_registry database: " + err.Error())
		return
	}
	// See "Important settings" section.
	deviceRegDB.SetConnMaxLifetime(time.Minute * 3)
	deviceRegDB.SetMaxOpenConns(10)
	deviceRegDB.SetMaxIdleConns(10)

	userRegDB, err = sql.Open("mysql", dbUser+":"+dbPass+"@tcp("+dbHost+":3306)/user_registry")
	if err != nil {
		log.Panic("Failed to open user_registry database: " + err.Error())
		return
	}
	userRegDB.SetConnMaxLifetime(time.Minute * 3)
	userRegDB.SetMaxOpenConns(10)
	userRegDB.SetMaxIdleConns(10)
}

func UpdateDevice(uuid string, userNamespace string, deviceName string) error {
	_, err := deviceRegDB.Exec(`UPDATE Device SET namespace="` + userNamespace +
		`", device_name="` + deviceName + `" WHERE uuid="` + uuid + `"`)
	return err
}

type Device struct {
	Namespace    string
	Uuid         string
	DeviceID     string
	DeviceType   string
	LastSeen     string
	DeviceName   string
	CreatedAt    string
	ActivatedAt  string
	DeviceStatus string
}

func GetDevicesLegacy(externalId string) ([]Device, error) {
	var userDevices []Device
	allDevices, err := GetDevices("default")

	if err != nil {
		return nil, err
	}

	for _, device := range allDevices {
		if strings.Contains(device.DeviceName, externalId) {
			userDevices = append(userDevices, device)
		}
	}
	return userDevices, err
}

func GetDevices(userNamespace string) ([]Device, error) {
	var (
		namespace     sql.NullString
		uuid          sql.NullString
		device_id     sql.NullString
		device_type   sql.NullString
		last_seen     sql.NullString
		device_name   sql.NullString
		created_at    sql.NullString
		activated_at  sql.NullString
		device_status sql.NullString
	)

	rows, err := deviceRegDB.Query(`SELECT * FROM Device WHERE namespace="` + userNamespace + `"`)
	if err != nil {
		log.Println("Error getting devices from database: " + err.Error())
		return nil, err
	}
	devices := []Device{}
	for rows.Next() {
		err := rows.Scan(&namespace,
			&uuid,
			&device_id,
			&device_type,
			&last_seen,
			&device_name,
			&created_at,
			&activated_at,
			&device_status)
		if err != nil {
			log.Println(err.Error())
			return nil, err
		}
		devices = append(devices, Device{namespace.String,
			uuid.String,
			device_id.String,
			device_type.String,
			last_seen.String,
			device_name.String,
			created_at.String,
			activated_at.String,
			device_status.String})

	}
	return devices, err
}

func GetActiveDevices(userNamespace string, intervalDays int) (int, error) {
	rows, err := deviceRegDB.Query(`select * from Device where namespace="` + userNamespace + `" and device_status <> 'NotSeen' and last_seen > DATE_SUB(curdate(), INTERVAL ` + strconv.Itoa(intervalDays) + ` Day);`)
	if err != nil {
		log.Println("Error getting devices from database: " + err.Error())

		return 0, err
	}
	// THis makes me want to die
	var i = 0
	for rows.Next() {
		i++
	}
	return i, nil
}

type User struct {
	id            string
	userNamespace string
	providerID    string // legacy name: externalUserID
	userID        string
	teamID        string
}

type DBI interface {
	GetNamespaceFromDB(providerID string) (string, error)
	InsertUser(user User) error
}

type MultiTenantV1DB struct {
	Name string
}

func (obj MultiTenantV1DB) GetNamespaceFromDB(externalID string) (string, error) {
	var (
		id             int
		providerID     sql.NullString
		userNamespace  sql.NullString
		externalUserID sql.NullString
		teamID         sql.NullString
	)

	rows, err := userRegDB.Query(`SELECT * FROM User WHERE providerID="` + externalID + `"`)
	if err != nil {
		log.Println("Error getting devices from database: " + err.Error())

		return "", err
	}
	for rows.Next() {
		err := rows.Scan(&id,
			&providerID,
			&userNamespace,
			&externalUserID,
			&teamID)
		if err != nil {
			log.Println(err.Error())

			return "", err
		}
	}
	return userNamespace.String, err
}

func (obj MultiTenantV1DB) InsertUser(user User) error {
	_, err := userRegDB.Exec(`INSERT into UserID VALUES(DEFAULT, "` +
		user.userNamespace + `", "` +
		user.providerID + `", "` +
		user.teamID + `")`)
	return err
}

type MultiTenantV2DB struct {
	Name string
}

func (obj MultiTenantV2DB) GetNamespaceFromDB(providerID string) (string, error) {
	var (
		id            int
		userNamespace sql.NullString
		extProviderID sql.NullString
		userID        sql.NullString
		teamID        sql.NullString
	)

	rows, err := userRegDB.Query(`SELECT * FROM User WHERE providerID="` + providerID + `"`)
	if err != nil {
		log.Println("Error getting devices from database: " + err.Error())

		return "", err
	}
	for rows.Next() {
		err := rows.Scan(&id,
			&userNamespace,
			&extProviderID,
			&userID,
			&teamID)
		if err != nil {
			log.Println(err.Error())

			return "", err
		}
	}
	if userNamespace.Valid {
		return userNamespace.String, nil
	}
	return "", err
}

func (obj MultiTenantV2DB) InsertUser(user User) error {
	_, err := userRegDB.Exec(`INSERT into User VALUES(DEFAULT, "` +
		user.userNamespace + `", "` +
		user.providerID + `", "` +
		user.userID + `", "` +
		user.teamID + `")`)
	return err
}

// I don't get it... shouldn't we just be using the sdk? Why aren't we doing that?

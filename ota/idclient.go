package ota

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/torizon-platform/idp"
)

var awsCredentialsPath = "/root/.aws/credentials"

const maxUsers = 5000

type IDPUserInfo = idp.User

const DevPool string = "dev"
const ProdPool string = "prod"

func getPoolID(pool string) string {
	if pool == DevPool {
		return "us-east-1_TWz2Tm658"
	} else if pool == ProdPool {
		return "us-east-1_Q5QdAyeZf"
	}
	return ""
}

func PrintUserInfo(info *idp.User) {
	if info == nil {
		fmt.Println("wtf, user info is nil!?")
		return
	}
	s, err := json.MarshalIndent(*info, "", "\t")
	if err != nil {
		fmt.Println("Failed to marshal user info into json. Error: " + err.Error())
	}
	fmt.Println(string(s[:]))
}

const idpConfFileName = "identity-provider.conf"

type idpConf struct {
	Envs       map[string]idp.IDPConfig `json:"envs"`
	CurrentEnv string                   `json:"current_env"`
}

type idpConfigFile map[string]idpConf

func GetIDPConfig(idpName string) (*idpConf, error) {
	file, err := ReadIDPConfigFile()
	if err != nil {
		return nil, err
	}
	if _, ok := (*file)[idpName]; !ok {
		return nil, errors.New("bad idp. No existing configuration found for: " + idpName)
	}
	idp := (*file)[idpName]
	return &idp, nil
}

// Just read in a file from home directory... i don't feel like making this fancy
func ReadIDPConfigFile() (*idpConfigFile, error) {
	homeDir, err := os.UserHomeDir()
	data, err := ioutil.ReadFile(homeDir + "/." + idpConfFileName)
	if err != nil {
		fmt.Println("Error reading keycloak configuration", err.Error())
		return nil, err
	}
	confFile := idpConfigFile{}
	err = json.Unmarshal(data, &confFile)
	return &confFile, err
}

func WriteIDPConfigFile(file idpConfigFile) error {
	fileBuf, err := json.Marshal(file)
	if err != nil {
		fmt.Println("Failed to parse file as json: " + err.Error())
		return err
	}
	homeDir, _ := os.UserHomeDir()
	return ioutil.WriteFile(homeDir+"/."+idpConfFileName, fileBuf, 0755)
}

func SetCurrentIDPEnv(idpName string, envName string) error {
	file, err := ReadIDPConfigFile()
	if err != nil {
		return err
	}

	// is idpName valid?
	if _, ok := (*file)[idpName]; !ok {
		return errors.New("No matching identity provider exists with name: " + idpName)
	}

	// is envName valid?
	if _, ok := (*file)[idpName].Envs[envName]; !ok {
		return errors.New("No matching env exists with name: " + envName)
	}
	// update the currentEnv
	idp := (*file)[idpName]
	idp.CurrentEnv = envName
	(*file)[idpName] = idp

	err = WriteIDPConfigFile(*file)
	return err
}

// getCurrentConfig returns the (config, envName, error)
func GetCurrentConfig(idpName string) (*idp.IDPConfig, string, error) {
	idpConf, err := GetIDPConfig(idpName)
	if err != nil {
		return nil, "", err
	}
	idpEnvs := &(*idpConf).Envs
	currentIdpEnv := (*idpConf).CurrentEnv
	outConfig := (*idpEnvs)[currentIdpEnv]
	return &outConfig, currentIdpEnv, nil
}

// a keycloak realm is = a cognito userPool
func CreateUser(idpName string, user IDPUserInfo) error {
	config, _, err := GetCurrentConfig(idpName)
	if err != nil {
		return err
	}
	// Hack in some last minute config overrides
	config.KC_NewUserRole = "torizon-free-tier"
	// If they are a toradex domain, we give them commercial tier
	emailDomain := strings.Split(user.Email, "@")[1]
	if emailDomain == "toradex.com" {
		config.KC_NewUserRole = "torizon-commercial-tier"
	}
	provider := idp.New(idpName, *config)
	return provider.RegisterUser(user)
}

func GetUser(idpName string, userID string) (*IDPUserInfo, error) {
	config, _, err := GetCurrentConfig(idpName)
	if err != nil {
		return nil, err
	}
	IDP := idp.New(idpName, *config)
	user, err := IDP.GetUserByID(userID)
	if err != nil {
		fmt.Println("Failed to get user back from " + idpName + ". Error: " + err.Error())
		return nil, err
	}
	return user, err
}

func GetUserByEmail(idpName string, userEmail string) (*IDPUserInfo, error) {
	config, _, err := GetCurrentConfig(idpName)
	if err != nil {
		return nil, err
	}
	//fmt.Println("Getting user (" + userEmail + "), from " + idpName + " (" + envName + ")")
	IDP := idp.New(idpName, *config)
	user, err := IDP.GetUser(userEmail)
	return user, err
}

func GetUserList(idpName string, userLimit int64) ([]*IDPUserInfo, error) {
	config, _, err := GetCurrentConfig(idpName)
	if err != nil {
		return nil, err
	}
	IDP := idp.New(idpName, *config)
	// if negative is specified, return max num users
	if userLimit < 0 {
		userLimit = maxUsers
	}
	users, err := IDP.GetUsers("", userLimit)
	if err != nil {
		fmt.Println("Failed to get user back from " + idpName + ". Error: " + err.Error())
		return nil, err
	}
	return users, err
}

// TODO, convert this to use the aws sdk and common configuration flow
func DeleteCognitoUser(providerID string, userPool string) error {
	app := "/bin/bash"
	arg0 := "-c"
	arg1 := "aws --region us-east-1 cognito-idp admin-delete-user --user-pool-id " + getPoolID(userPool) + " --username " + providerID

	cmd := exec.Command(app, arg0, arg1)
	var out bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &stderr

	err := cmd.Run()
	if err != nil {
		fmt.Println(fmt.Sprint(err) + ": " + stderr.String())
		fmt.Println("\n\n Did you forget to run `aws configure`?")
		return err
	}
	//fullUserJSON := out.String()
	fmt.Println("Result: " + out.String())
	return nil
}

func UpdateUserNamespace(idpName string, userEmail string, namespace string) error {
	userInfo := idp.User{Attributes: idp.UserAttributes{Namespace: namespace}}
	config, _, err := GetCurrentConfig(idpName)
	if err != nil {
		return err
	}
	IDP := idp.New(idpName, *config)
	return IDP.UpdateUser(userEmail, userInfo)
}

package ota

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"net/http"
)

func AddNewBackendUser(providerID string) error {
	client := &http.Client{}

	req, err := http.NewRequest("POST", "http://credentials/users", nil)
	req.Header.Add("external-id", providerID)
	req.Header.Add("provider-id", providerID)

	resp, err := client.Do(req)
	if err != nil {
		log.Printf("Failed POST Request to the credentials service (create user)\n")
		return err
	}

	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)

	if resp.StatusCode != 200 {
		log.Printf("Got bad response from credentials service: Got" + resp.Status)
		return errors.New("Got non 200 response from credentials service: Got" + resp.Status)
	}
	return nil
}

func DeleteBackendUser(providerID string) error {
	// Do all the things to delete a user from the OTA backend.
	// Really just remove the references from tuf_reposerver.repo_namespaces, and user_registry.User.
	// Ben Says: We potentially leave a bunch of dangling devices and MTU updates... Weird stuff could happen if that MTU was referenced elsewhere.
	fmt.Println("Deleting backend users is not yet supported... Ben is waiting till we migrate to using the sdk")
	return nil
}
